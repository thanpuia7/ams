<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobapplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobapplications', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('gender')->nullable();
            $table->string('religion')->nullable();
            $table->string('dob')->nullable();
            $table->string('fathersname')->nullable();
            $table->string('caste')->nullable();
            $table->string('state')->nullable();
            $table->string('district')->nullable();
            $table->string('paddress')->nullable();
            $table->string('caddress')->nullable();

            $table->string('jobapplys')->nullable();
            $table->string('examination1')->nullable();
            $table->string('university1')->nullable();
            $table->string('percentage1')->nullable();
            $table->string('subject1')->nullable();
            $table->string('year1')->nullable();
            $table->string('examination2')->nullable();
            $table->string('university2')->nullable();
            $table->string('percentage2')->nullable();
            $table->string('subject2')->nullable();
            $table->string('year2')->nullable();
            $table->string('examination3')->nullable();
            $table->string('university3')->nullable();
            $table->string('percentage3')->nullable();

            $table->string('subject3')->nullable();
            $table->string('year3')->nullable();
            $table->string('examination4')->nullable();
            $table->string('university4')->nullable();
            $table->string('percentage4')->nullable();
            $table->string('subject4')->nullable();
            $table->string('year4')->nullable();

            $table->string('nameoftraining1')->nullable();
            $table->string('trainingperiod1')->nullable();
            $table->string('traininginstitute1')->nullable();
            $table->string('nameoftraining2')->nullable();
            $table->string('trainingperiod2')->nullable();
            $table->string('traininginstitute2')->nullable();
           


            $table->string('nameoftraining3')->nullable();
            $table->string('trainingperiod3')->nullable();
            $table->string('traininginstitute3')->nullable();
            $table->string('expperiod1')->nullable();
            $table->string('expjoindate1')->nullable();
            $table->string('exppostheld1')->nullable();
            $table->string('expemployer1')->nullable();

            $table->string('explastbasic1')->nullable();
            $table->string('expacheivement1')->nullable();
            $table->string('expperiod2')->nullable();
            $table->string('expjoindate2')->nullable();
            $table->string('exppostheld2')->nullable();
            $table->string('expemployer2')->nullable();
            $table->string('explastbasic2')->nullable();

            $table->string('expacheivement2')->nullable();
            $table->string('expperiod3')->nullable();
            $table->string('expjoindate3')->nullable();
            $table->string('exppostheld3')->nullable();
            $table->string('expemployer3')->nullable();
            $table->string('explastbasic3')->nullable();
            $table->string('expacheivement3')->nullable();

            $table->string('oexppostheld1')->nullable();
            $table->string('oexporganiation1')->nullable();
            $table->string('oexeperiod1')->nullable();
            $table->string('oexppostheld2')->nullable();
            $table->string('oexporganiation2')->nullable();
            $table->string('oexeperiod2')->nullable();
            $table->string('oexppostheld3')->nullable();

            $table->string('oexporganiation3')->nullable();
            $table->string('oexeperiod3')->nullable();

            $table->string('areaofspecialization1')->nullable();
            $table->string('areaofspecialization2')->nullable();
            $table->string('areaofspecialization3')->nullable();
            $table->string('areaofspecialization4')->nullable();
            $table->string('url')->nullable();
            $table->string('registrationNo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobapplications');
    }
}
