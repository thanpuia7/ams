<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBackenddevelopersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backenddevelopers', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('name')->nullable();
            $table->string('passportURL')->nullable();
            $table->string('email')->nullable();
            $table->string('gender')->nullable();
            $table->string('phone')->nullable();
            $table->string('whatsapp')->nullable();          
            $table->string('religion')->nullable();
            $table->string('dob')->nullable();
            $table->string('fathersname')->nullable();
            $table->string('caste')->nullable();
            $table->string('state')->nullable();
            $table->string('district')->nullable();
            $table->string('caddress')->nullable();



            $table->string('subject1')->nullable();
            $table->string('university1')->nullable();
            $table->string('year1')->nullable();
            $table->string('marksheet1URL')->nullable();
            $table->string('passingcertificate1URL')->nullable();

            $table->string('subject2')->nullable();
            $table->string('university2')->nullable();
            $table->string('year2')->nullable();
            $table->string('marksheet2URL')->nullable();
            $table->string('passingcertificate2URL')->nullable();

            $table->string('subject3')->nullable();
            $table->string('university3')->nullable();
            $table->string('year3')->nullable();
            $table->string('marksheet3URL')->nullable();
            $table->string('passingcertificate3URL')->nullable();

            $table->string('subject4')->nullable();
            $table->string('university4')->nullable();
            $table->string('year4')->nullable();
            $table->string('marksheet4URL')->nullable();
            $table->string('passingcertificate4URL')->nullable();




            $table->string('coursename1')->nullable();
            $table->string('period1')->nullable();
            $table->string('institute1')->nullable();

            $table->string('coursename2')->nullable();
            $table->string('period2')->nullable();
            $table->string('institute2')->nullable();

            $table->string('coursename3')->nullable();
            $table->string('period3')->nullable();
            $table->string('institute3')->nullable();




            $table->string('postname1')->nullable();
            $table->string('organizationname1')->nullable();
            $table->string('duration1')->nullable();
            $table->string('experiencecertificate1URL')->nullable();
           
            $table->string('postname2')->nullable();
            $table->string('organizationname2')->nullable();
            $table->string('duration2')->nullable();
            $table->string('experiencecertificate2URL')->nullable();
            
            $table->string('postname3')->nullable();
            $table->string('organizationname3')->nullable();
            $table->string('duration3')->nullable();
            $table->string('experiencecertificate3URL')->nullable();
            
            $table->string('postname4')->nullable();
            $table->string('organizationname4')->nullable();
            $table->string('duration4')->nullable();
            $table->string('experiencecertificate4URL')->nullable();
            
            $table->string('postname5')->nullable();
            $table->string('organizationname5')->nullable();
            $table->string('duration5')->nullable();
            $table->string('experiencecertificate5URL')->nullable();
           
          

            $table->string('technicalknowledges')->nullable();
            $table->string('portfolioURL')->nullable();
            $table->string('jobapplys')->nullable();
            $table->string('registrationNo')->nullable();
            $table->string('status')->nullable();
           
  
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('backenddevelopers');
    }
}
