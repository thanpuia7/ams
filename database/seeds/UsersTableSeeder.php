<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([

            'role_id' => 1,
            'name' => 'Admin',
            'email' => 'superadmin@mail.com',
            'password' => Hash::make('password')
           


        ]);

        
    DB::table('users')->insert([

        'role_id' => 1,
        'name' => 'Dr Lalthlamuana',
        'email' => 'muana.mizo@gmail.com',
        'password' => Hash::make('password123')           

    ]);

            
    DB::table('users')->insert([

        'role_id' => 1,
        'name' => 'Isaac Zothanpuia',
        'email' => 'pm.msegs@mizoram.gov.in',
        'password' => Hash::make('password123')           

    ]);

            DB::table('users')->insert([

                'role_id' => 1,
                'name' => 'Zonuna Zote',
                'email' => 'zonuna.msegs@gmail.com',
                'password' => Hash::make('password123')           

        ]);

        
        DB::table('users')->insert([

            'role_id' => 1,
            'name' => 'Thanpuia Chhangte',
            'email' => 'thanpuia46@gmail.com',
            'password' => Hash::make('password123')           

    ]);

    DB::table('users')->insert([

        'role_id' => 1,
        'name' => 'Thanpuia Fanai',
        'email' => 'thanpuia7@gmail.com',
        'password' => Hash::make('password123')           

    ]);

    DB::table('users')->insert([

        'role_id' => 1,
        'name' => 'Afeli',
        'email' => 'afeli@gmail.com',
        'password' => Hash::make('password123')           

    ]);

    }
}
