@extends('layouts/app')
@section('title','Home')

@section('content')
<div style ="background-color: rgba(241, 241, 241);" class="position-relative">

    <!-- <div class="d-none d-lg-block "> -->
    <div class=" ">
        <div style="height: 200px; background-color: rgba(73, 114, 222);" class="">   
            <CENTER> 
                <div class="container">
                    <h4 class="text-white pt-5">APPLICATION FOR EMPLOYMENT</H4>
                    <br>
                </div>
            </center>
        </div>
    </div>

<!-- <div style ="background-color: rgba(13, 56, 130);" class=" d-lg-none">

<p class="text-white px-3 py-3">He portal hi Mizoram mi leh sa dik tak, Mizoram pawna Covid-19 hrileng vanga tangkhang ten, Mizoram Sawrkar hnena an in report theihna tura buatsaih a ni e (Mizoram chhunga awm mek te tan a ni lo! )</p>

</div> -->

    <div class="position-relative">
        <div class="position-absolute" style="left:0; right:0; top: -100px;">
            <div  style ="background-color: rgba(255, 255, 255);" class=" col-xs-12 col-md-6 container rounded border shadow my-md-4 ">

                @if (Session::has('userexist'))
                <div class="alert alert-info">
                    <ul>
                        <li>{{ Session::get('userexist') }}</li>
                    </ul>
                </div>
                @endif

                <form  class="needs-validation mx-sm-4 mt-3 mt-sm-5 mb-5" method="POST" action="{{ route('strandedSubmit') }}">
                @csrf

                    <div  class="form-row ">
                        <div class="col-md-6 mb-2 pr-2">
                            <label  for="name">Name of Applicant</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="" >
                        <p class=" text-danger">{{$errors->first('name')}}</p>
                        </div>
                        <div class="col-md-6 mb-2 pl-2">
                            <label for="name">Email</label>
                            <input type="text" name="email" class="form-control" id="email" placeholder="" >
                        <p class=" text-danger">{{$errors->first('email')}}</p>
                        </div>

                    </div>

                    <div class="form-row" >
                        <div class="col-md-6 mb-2 pr-2">
                            <label for="phone">Contact Number *</label>
                            <input type="text" name="phone" class="form-control" id="phone" placeholder="" >
                            <p class=" text-danger">{{$errors->first('phone')}}</p>
                        </div>

                        
                        <div class="col-md-6 mb-2 pl-2">
                            <label for="gender">Gender *</label>
                            <select name ="gender" class="form-control">

                                        <option value="" disabled selected>Select</option>
                                    
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                
                                </select>

                                <p class="text-danger">{{$errors->first('gender')}}</p>
                        </div>
                    </div>


                    <div  class="form-row ">
                        <div class="col-md-6 mb-2 pr-2">
                            <label for="name">Religion</label>
                            <input type="text" name="religion" class="form-control" id="religion" placeholder="" >
                        <p class=" text-danger">{{$errors->first('religion')}}</p>
                        </div>
                        <div class="col-md-6 mb-2 pl-2">
                            <label for="name">Date of Birth</label>
                            <input type="date" name="dob" class="form-control" id="dob" placeholder="" >
                        <p class=" text-danger">{{$errors->first('dob')}}</p>
                        </div>

                    </div>

                    <div  class="form-row ">
                        <div class="col-md-6 mb-2 pr-2">
                            <label for="name">Father's or Mother's Name</label>
                            <input type="text" name="fathersname" class="form-control" id="fathersname" placeholder="" >
                        <p class=" text-danger">{{$errors->first('fathersname')}}</p>
                        </div>
                        <div class="col-md-6 mb-2 pl-2">
                            <label for="mizoramdistrict">Caste *</label>
                            <select name ="caste" id="caste" class="form-control" >
                                        <option value="" disabled selected>Select </option>
                                
                                        <option value="Scheduled Tribe">Scheduled Tribe</option>
                                        <option value="Scheduled Caste">Scheduled Caste</option>
                                        <option value="OBC">OBC</option>
                                        <option value="General">General</option>
                                
                                </select>

                                <p class=" text-danger">{{$errors->first('caste')}}</p>
                        </div>

                    </div>

                    <div  class="form-row ">
                        <div class="col-md-6 mb-2 pr-2">
                            <label for="name">State</label>
                            <input type="text" name="state" class="form-control" id="state" placeholder="" >
                        <p class=" text-danger">{{$errors->first('state')}}</p>
                        </div>
                        <div class="col-md-6 mb-2 pl-2">
                            <label for="name">District</label>
                            <input type="text" name="district" class="form-control" id="district" placeholder="" >
                        <p class=" text-danger">{{$errors->first('district')}}</p>
                        </div>

                    </div>

                    
                    <div  class="form-row ">
                        <div class="col-md-6 mb-2 pr-2">
                            <label for="name">Permanent Address </label>
                            <textarea  name="paddress" class="form-control" rows="3" id="paddress" placeholder="" ></textarea>
                        <p class=" text-danger">{{$errors->first('paddress')}}</p>
                        </div>
                        <div class="col-md-6 mb-2 pl-2">
                            <label for="name">Correspondence Address</label>
                            <textarea name="caddress" class="form-control" rows="3" id="caddress" placeholder="" ></textarea>
                        <p class=" text-danger">{{$errors->first('caddress')}}</p>
                        </div>

                    </div>

                    <br>
                    <p >Select the post you want to apply for:</p>

                    <div class = "border pl-2 pr-2 pt-2  mb-2">
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    
                                    <td class = "text-muted" scope="col">Tick</td>
                                    <td class = "text-muted" scope="col">No.</td>
                                    <td class = "text-muted" scope="col">JOB POST</td>
                                    <td class = "text-muted" scope="col">Salary </td>
                                    <td class = "text-muted" scope="col">Requirement</td>
                                </tr>
                            </thead>
                            <tbody>
                               
                                <tr>
                                <td colspan="5">
                                <center>Enterpreneurship Development Programme</center>
                                </td>
                                </tr>
                                
                                <tr>
                                    
                                    
                                    <td>
                                        <input type="checkbox" name="jobapply[]" class="checkbox px-2 py-2" value="1" id="name" placeholder="" >
                                        <!-- <p class=" text-danger">{{$errors->first('name')}}</p> -->
                                    </td>
                                    <td class = "text-muted" scope="row">1</td>

                                    <td>
                                    <label class = "text-muted" for="name">Co-Ordinator</label>
                                    </td>

                                    <td>
                                    <label  class = "text-muted" for="name">28100</label>
                                    </td>
                                    <td>
                                    <label class = "text-muted" for="name"> - </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="jobapply[]" class="checkbox px-2 py-2" value="2" id="name" placeholder="" >
                                        <!-- <p class=" text-danger">{{$errors->first('name')}}</p> -->
                                    </td>
                                    <td class = "text-muted" scope="row">2</td>

                                    <td>
                                    <label class = "text-muted" for="name">Mentor (Management)</label>
                                    </td>

                                    <td>
                                    <label class = "text-muted" for="name">40700</label>
                                    </td>
                                    <td>
                                    <label class = "text-muted" for="name"> - </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="jobapply[]" class="checkbox px-2 py-2" value="3" id="name" placeholder="" >
                                        <!-- <p class=" text-danger">{{$errors->first('name')}}</p> -->
                                    </td>
                                    <td class = "text-muted" scope="row">3</td>

                                    <td>
                                    <label class = "text-muted"  for="name">Mentor (Technical)</label>
                                    </td>

                                    <td>
                                    <label class = "text-muted" for="name">40700</label>
                                    </td>
                                    <td>
                                    <label class = "text-muted" for="name"> - </label>
                                    </td>
                                </tr>

                                <tr>
                                <td colspan="5">
                                <center>SDC</center>
                                </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        <input type="checkbox" name="jobapply[]" value="4" class="checkbox px-2 py-2" id="name" placeholder="" >
                                        <!-- <p class=" text-danger">{{$errors->first('name')}}</p> -->
                                    </td>
                                    <td class = "text-muted" scope="row">4</td>

                                    <td>
                                    <label class = "text-muted" for="name">Project Manager</label>
                                    </td>

                                    <td>
                                    <label class = "text-muted" for="name">50000</label>
                                    </td>
                                    <td>
                                    <label class = "text-muted" for="name"> - </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="jobapply[]" value="5" class="checkbox px-2 py-2" id="name" placeholder="" >
                                        <!-- <p class=" text-danger">{{$errors->first('name')}}</p> -->
                                    </td>
                                    <td class = "text-muted" scope="row">5</td>

                                    <td>
                                    <label class = "text-muted" for="name">Datatbase Administrator</label>
                                    </td>

                                    <td>
                                    <label class = "text-muted" for="name">40700</label>
                                    </td>
                                    <td>
                                    <label class = "text-muted" for="name"> - </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="jobapply[]" value="6" class="checkbox px-2 py-2" id="name" placeholder="" >
                                        <!-- <p class=" text-danger">{{$errors->first('name')}}</p> -->
                                    </td>
                                    <td class = "text-muted" scope="row">6</td>

                                    <td>
                                    <label class = "text-muted" for="name">System  Administrator</label>
                                    </td>

                                    <td>
                                    <label class = "text-muted" for="name">40700</label>
                                    </td>
                                    <td>
                                    <label class = "text-muted" for="name"> - </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="jobapply[]" value="7" class="checkbox px-2 py-2" id="name" placeholder="" >
                                        <!-- <p class=" text-danger">{{$errors->first('name')}}</p> -->
                                    </td>
                                    <td class = "text-muted" scope="row">7</td>

                                    <td>
                                    <label class = "text-muted" for="name">Network & Security Administrator</label>
                                    </td>

                                    <td>
                                    <label class = "text-muted" for="name">40700</label>
                                    </td>
                                    <td>
                                    <label class = "text-muted" for="name"> - </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="jobapply[]" value="8" class="checkbox px-2 py-2" id="name" placeholder="" >
                                        <!-- <p class=" text-danger">{{$errors->first('name')}}</p> -->
                                    </td>
                                    <td class = "text-muted" scope="row">8</td>

                                    <td>
                                    <label class = "text-muted" for="name">BMS Administrator</label>
                                    </td>

                                    <td>
                                    <label class = "text-muted" for="name">28100</label>
                                    </td>
                                    <td>
                                    <label class = "text-muted" for="name"> - </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="jobapply[]" value="9" class="checkbox px-2 py-2" id="name" placeholder="" >
                                        <!-- <p class=" text-danger">{{$errors->first('name')}}</p> -->
                                    </td>
                                    <td class = "text-muted" scope="row">9</td>

                                    <td>
                                    <label class = "text-muted" for="name">Help Desk</label>
                                    </td>

                                    <td>
                                    <label class = "text-muted" for="name">28100</label>
                                    </td>
                                    <td>
                                    <label class = "text-muted" for="name">Graduate in Computer CS/IT/CA or 3 yrs Diploma in ECE/IT/CS with 2-3 yrs exp</label>
                                    </td>
                                </tr>


                                <tr>
                                <td colspan="5">
                                <center>CCTNS</center>
                                </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        <input type="checkbox" name="jobapply[]" value="10" class="checkbox px-2 py-2" id="name" placeholder="" >
                                        <!-- <p class=" text-danger">{{$errors->first('name')}}</p> -->
                                    </td>
                                    <td class = "text-muted" scope="row">10</td>

                                    <td>
                                    <label class = "text-muted" for="name">Senior Management Consultant</label>
                                    </td>

                                    <td>
                                    <label class = "text-muted" for="name">50000</label>
                                    </td>
                                    <td>
                                    <label class = "text-muted" for="name">(CS/ECE/IT)/MCA/MSc (CS/IT) with 5yrs exp in IT Advisory, IT Services, IT Consulting in reputed Company </label>
                                    </td>
                                </tr>

                                <tr>
                                <td colspan="5">
                                <center>SSDG/SP</center>
                                </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        <input type="checkbox" name="jobapply[]" value="11" class="checkbox px-2 py-2" id="name" placeholder="" >
                                        <!-- <p class=" text-danger">{{$errors->first('name')}}</p> -->
                                    </td>
                                    <td class = "text-muted" scope="row">11</td>

                                    <td>
                                    <label class = "text-muted" for="name">UI/UX Designer</label>
                                    </td>

                                    <td>
                                    <label class = "text-muted" for="name">40700</label>
                                    </td>
                                    <td>
                                    <label class = "text-muted" for="name">BE (CS/IT)/MCA/MSc (CS/IT) with 2-3 yrs exp in UI/UX Deign</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="jobapply[]" value="12" class="checkbox px-2 py-2" id="name" placeholder="" >
                                        <!-- <p class=" text-danger">{{$errors->first('name')}}</p> -->
                                    </td>
                                    <td class = "text-muted" scope="row">12</td>

                                    <td>
                                    <label class = "text-muted" for="name">Software Developer (Front-End)</label>
                                    </td>

                                    <td>
                                    <label class = "text-muted" for="name">40700</label>
                                    </td>
                                    <td>
                                    <label class = "text-muted" for="name">BE (CS/IT)/MCA/MSc (CS/IT) with 2-3 yrs exp in Frontend</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="jobapply[]" value="13" class="checkbox px-2 py-2" id="name" placeholder="" >
                                        <!-- <p class=" text-danger">{{$errors->first('name')}}</p> -->
                                    </td>
                                    <td class = "text-muted" scope="row">13</td>

                                    <td>
                                    <label class = "text-muted" for="name">Software Developer (Back-End)</label>
                                    </td>

                                    <td>
                                    <label class = "text-muted" for="name">40700</label>
                                    </td>
                                    <td>
                                    <label class = "text-muted" for="name"> BE (CS/IT)/MCA/MSc (CS/IT) with 2-3 yrs exp in Backend</label>
                                    </td>
                                </tr>

                                <tr>
                                <td colspan="5">
                                <center>EODB</center>
                                </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        <input type="checkbox" name="jobapply[]" value= "14" class="checkbox px-2 py-2" id="name" placeholder="" >
                                        <!-- <p class=" text-danger">{{$errors->first('name')}}</p> -->
                                    </td>
                                    <td class = "text-muted" scope="row">14</td>

                                    <td>
                                    <label class = "text-muted"  for="name">UI/UX Designer</label>
                                    </td>

                                    <td>
                                    <label class = "text-muted" for="name">40700</label>
                                    </td>
                                    <td>
                                    <label class = "text-muted" for="name">BE (CS/IT)/MCA/MSc (CS/IT) with 2-3 yrs exp in UI/UX Deign</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="jobapply[]" value="15" class="checkbox px-2 py-2" id="name" placeholder="" >
                                        <!-- <p class=" text-danger">{{$errors->first('name')}}</p> -->
                                    </td>
                                    <td class = "text-muted" scope="row">15</td>

                                    <td>
                                    <label class = "text-muted" for="name">Software Developer (Front-End)</label>
                                    </td>

                                    <td>
                                    <label class = "text-muted" for="name">40700</label>
                                    </td>
                                    <td>
                                    <label class = "text-muted" for="name">BE (CS/IT)/MCA/MSc (CS/IT) with 2-3 yrs exp in Frontend</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="jobapply[]" value="16" class="checkbox border" id="name" placeholder="" >
                                        <!-- <p class=" text-danger">{{$errors->first('name')}}</p> -->
                                    </td>
                                    <td class = "text-muted" scope="row">16</td>

                                    <td>
                                    <label class = "text-muted" for="name">Software Developer (Back-End)</label>
                                    </td>

                                    <td>
                                    <label class = "text-muted" for="name">40700</label>
                                    </td>
                                    <td>
                                    <label class = "text-muted" for="name"> BE (CS/IT)/MCA/MSc (CS/IT) with 2-3 yrs exp in Backend</label>
                                    </td>
                                </tr>






                            </tbody>
                        </table>
                    </div>

                    <br>

                    <p >Educational Qualification:</p>

                    <div class = "border pl-2 pr-2 pt-2  mb-2">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                <td scope="col"></td>
                                <td class = "text-muted" scope="col">Examination</td>
                                <td class = "text-muted" scope="col">University</td>
                                <td class = "text-muted" scope="col">Percentage</td>
                                <td class = "text-muted" scope="col">Subjects</td>
                                <td class = "text-muted" scope="col">Year</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class = "text-muted" scope="row">1</td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class = "text-muted" scope="row">2</td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td class = "text-muted" scope="row">3</td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>


                    <br>
                    <p >Professional Training:</p>

                    <div class = "border pl-2 pr-2 pt-2  mb-2">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <td scope="col"></td>
                                    <td class = "text-muted" scope="col">Name of Training Program</td>
                                    <td class = "text-muted" scope="col">Period</td>
                                    <td class = "text-muted" scope="col">Institute</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class = "text-muted" scope="row">1</td>

                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td class = "text-muted" scope="row">2</td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td class = "text-muted" scope="row">3</td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>


                    <br>
                    <p >Work Experience with particular post held:</p>

                    <div class = "border pl-2 pr-2 pt-2  mb-2">
                        <table class="table table-borderless table-responsive " >
                            <thead style="width: 2000px !important;">
                                <tr >
                                    <td class = "text-muted " scope="col">Period</td>
                                    <td  class = "text-muted " scope="col-1">Join Date</td>
                                    <td  class = "text-muted " scope="col">Post Held</td>
                                    <td class = "text-muted " scope="col">Employer</td>
                                    <td class = "text-muted " scope="col-4" >Last Basic Pay</td>
                                    <td class = "text-muted " scope="col">Achievement</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="text" name="name" class="form-control w-auto" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>

                                    <td>
                                        <input type="date" name="name" class="form-control w-auto" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="name" class="form-control w-auto" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="name" class="form-control w-auto" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="name" class="form-control w-auto" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                    <td>
                                        <input type="textarea" name="name" class="form-control w-auto" id="name" placeholder="" rows="2">
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                </tr>
                                <tr>
                                <td>
                                        <input type="text" name="name" class="form-control w-auto" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>

                                    <td>
                                        <input type="date" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>

                                </tr>

                                <tr>
                                <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>

                                    <td>
                                        <input type="date" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>



                    <br>
                    <p >Ay Other Experience, If Any:</p>

                    <div class = "border pl-2 pr-2 pt-2  mb-2">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <td scope="col"></td>
                                    <td class = "text-muted" scope="col">Post Held</td>
                                    <td class = "text-muted" scope="col">Organization</td>
                                    <td class = "text-muted" scope="col">Period</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class = "text-muted" scope="row">1</td>

                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td class = "text-muted" scope="row">2</td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td class = "text-muted" scope="row">3</td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="name" class="form-control" id="name" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>


                    <br>
                    <p >Technical/Management:</p>

                    <div class = "border pl-2 pr-2 pt-2  mb-2">
                        <table class="table table-borderless table-responsive">
                            <thead>
                                <tr>
                                <td scope="col"></td>
                                <td class = "text-muted" scope="col">Area of Specialization</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr> 
                                <td class = "text-muted" scope="row">1</td>
                                    <td>
                                        <input type="text" name="name" class="form-control w-auto" id="name" placeholder="" size="50">
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                </tr>

                                <tr> 
                                <td class = "text-muted" scope="row">2</td>
                                    <td>
                                        <input type="text" name="name" class="form-control w-auto" id="name" placeholder="" size="50">
                                        <p class=" text-danger">{{$errors->first('name')}}</p>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>

                    <div class="col-md-6 py-3 pr-2">
                    <label  for="name">Upload your passport photo:</label>
                        <input type="file" name="file" class="">
                    </div>
                       
                    <div>
                        <label for="vehicle1" class="py-3 ">
                            <input type="checkbox" name="agree" value="1">
                             I certify that the foregoing information is correct and complete to the best of my knowledge and belief.
                        </label><br>
                    </div>
  
                    <div class="col-md-6">
                        <label class="ohnohoney" for="email"></label>
                        <input class="ohnohoney" autocomplete="off" type="email" id="email" name="email" placeholder="Your e-mail here">
                    </div>

                    <button class="btn btn-primary" type="submit">Submit form</button>

                </form>

            </div>

            <div id="contactus" style="height: auto; background-color: rgba(13, 56, 130);" >
                <div class="px-5 pt-4 text-white">
                    <div class="text-center pb-3">
                        <h4>Contact Us</h4>          
                    </div>
                </div>
            </div>



            <div>
                    <p class="py-3 text-xs font-weight-light text-center">Crafted with care by <a href="#" class="text-red">Mizoram State e-Governance Society (MSeGS)</a>, hosted by department of ICT, Government of Mizoram</p>
            </div>



        </div>
    </div>

</div>
@endsection