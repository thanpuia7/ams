
@extends('adminlte::page')
@section('title','Dashboard')

@section('content')
  <div class="row" >
   
  <div class="col-xl-4 col-md-6">
      <div class="card social-card shadow-lg " style="background-color:#01A9AC;">
        <div class="card-block mx-4 my-2">
        <center><h5 class="text-white pt-1 pb-1">All Applicants</h5></center>
          <div class="row align-items-center  pb-3">
            <div class="col-auto bg-white rounded">
              <a href="{{ route('applicantList') }}" ><button class="btn btn-info mt-2 mb-2 ">View</button></a>
            </div>
            <div class="col text-white py-2 pl-3 border rounded  shadow-lg ml-3 ">
              <h6>Number of Applicants: {{$total}}</h6>
              <h6>Pending: {{$pending}}</h6>
              <h6>Accepted: {{$accept}}</h6>
              <h6>Rejected: {{$reject}}</h6>
            </div>
          </div>
        </div>       
      </div>
    </div>

    <div class="col-xl-4 col-md-6">
      <div class="card social-card shadow-lg " style="background-color:#01A9AC;">
        <div class="card-block mx-4 my-2">
        <center><h5 class="text-white pt-1 pb-1">Project Manager</h5></center>
          <div class="row align-items-center  pb-3">
            <div class="col-auto bg-white rounded">
              <a href="{{ route('pmList') }}"><button class="btn btn-info mt-2 mb-2 ">View</button></a>
            </div>
            <div class="col text-white py-2 pl-3 border rounded  shadow-lg ml-3 ">
              <h6>Number of Applicants: {{$total1}}</h6>
              <h6>Pending: {{$pending1}}</h6>
              <h6>Accepted: {{$accept1}}</h6>
              <h6>Rejected: {{$reject1}}</h6>
            </div>
          </div>
        </div>       
      </div>
    </div>

    <div class="col-xl-4 col-md-6">
      <div class="card social-card shadow-lg " style="background-color:#01A9AC;">
        <div class="card-block mx-4 my-2">
        <center><h6 class="text-white pt-2 pb-1">Senior Management Consultant</h6></center>
          <div class="row align-items-center  pb-3">
            <div class="col-auto bg-white rounded">
              <a href="{{ route('smcList') }}" ><button class="btn btn-info mt-2 mb-2 ">View</button></a>
            </div>
            <div class="col text-white py-2 pl-3 border rounded  shadow-lg ml-3 ">
              <h6>Number of Applicants: {{$total2}}</h6>
              <h6>Pending: {{$pending2}}</h6>
              <h6>Accepted: {{$accept2}}</h6>
              <h6>Rejected: {{$reject2}}</h6>
            </div>
          </div>
        </div>       
      </div>
    </div>
    
 </div>

 <div class="row" >
   
  <div class="col-xl-4 col-md-6">
      <div class="card social-card shadow-lg " style="background-color:#01A9AC;">
        <div class="card-block mx-4 my-2">
        <center><h5 class="text-white pt-1 pb-1">Database Administrator</h5></center>
          <div class="row align-items-center  pb-3">
            <div class="col-auto bg-white rounded">
              <a href="{{ route('daList') }}"><button class="btn btn-info mt-2 mb-2 ">View</button></a>
            </div>
            <div class="col text-white py-2 pl-3 border rounded  shadow-lg ml-3 ">
              <h6>Number of Applicants: {{$total3}}</h6>
              <h6>Pending: {{$pending3}}</h6>
              <h6>Accepted: {{$accept3}}</h6>
              <h6>Rejected: {{$reject3}}</h6>
            </div>
          </div>
        </div>       
      </div>
    </div>

    <div class="col-xl-4 col-md-6">
      <div class="card social-card shadow-lg " style="background-color:#01A9AC;">
        <div class="card-block mx-4 my-2">
        <center><h5 class="text-white pt-1 pb-1">System Administrator</h5></center>
          <div class="row align-items-center  pb-3">
            <div class="col-auto bg-white rounded">
              <a href="{{ route('saList') }}"><button class="btn btn-info mt-2 mb-2 ">View</button></a>
            </div>
            <div class="col text-white py-2 pl-3 border rounded  shadow-lg ml-3 ">
              <h6>Number of Applicants: {{$total4}}</h6>
              <h6>Pending: {{$pending4}}</h6>
              <h6>Accepted: {{$accept4}}</h6>
              <h6>Rejected: {{$reject4}}</h6>
            </div>
          </div>
        </div>       
      </div>
    </div>

    <div class="col-xl-4 col-md-6">
      <div class="card social-card shadow-lg " style="background-color:#01A9AC;">
        <div class="card-block mx-4 my-2">
        <center><h6 class="text-white pt-2 pb-1">Network & Security Administrator</h6></center>
          <div class="row align-items-center  pb-3">
            <div class="col-auto bg-white rounded">
              <a href="{{ route('nsaList') }}" ><button class="btn btn-info mt-2 mb-2 ">View</button></a>
            </div>
            <div class="col text-white py-2 pl-3 border rounded  shadow-lg ml-3 ">
              <h6>Number of Applicants: {{$total5}}</h6>
              <h6>Pending: {{$pending5}}</h6>
              <h6>Accepted: {{$accept5}}</h6>
              <h6>Rejected: {{$reject5}}</h6>
            </div>
          </div>
        </div>       
      </div>
    </div>
    
 </div>

 <div class="row" >
   
  <div class="col-xl-4 col-md-6">
      <div class="card social-card shadow-lg " style="background-color:#01A9AC;">
        <div class="card-block mx-4 my-2">
        <center><h5 class="text-white pt-1 pb-1">UI/UX Designer</h5></center>
          <div class="row align-items-center  pb-3">
            <div class="col-auto bg-white rounded">
              <a href="{{ route('uxuiList') }}"><button class="btn btn-info mt-2 mb-2 ">View</button></a>
            </div>
            <div class="col text-white py-2 pl-3 border rounded  shadow-lg ml-3 ">
              <h6>Number of Applicants: {{$total6}}</h6>
              <h6>Pending: {{$pending6}}</h6>
              <h6>Accepted: {{$accept6}}</h6>
              <h6>Rejected: {{$reject6}}</h6>
            </div>
          </div>
        </div>       
      </div>
    </div>

    <div class="col-xl-4 col-md-6">
      <div class="card social-card shadow-lg " style="background-color:#01A9AC;">
        <div class="card-block mx-4 my-2">
        <center><h5 class="text-white pt-1 pb-1">Software Developer (Frontend)</h5></center>
          <div class="row align-items-center  pb-3">
            <div class="col-auto bg-white rounded">
              <a href="{{ route('sdFList') }}" ><button class="btn btn-info mt-2 mb-2 ">View</button></a>
            </div>
            <div class="col text-white py-2 pl-3 border rounded  shadow-lg ml-3 ">
              <h6>Number of Applicants: {{$total7}}</h6>
              <h6>Pending: {{$pending7}}</h6>
              <h6>Accepted: {{$accept7}}</h6>
              <h6>Rejected: {{$reject7}}</h6>
            </div>
          </div>
        </div>       
      </div>
    </div>

    <div class="col-xl-4 col-md-6">
      <div class="card social-card shadow-lg " style="background-color:#01A9AC;">
        <div class="card-block mx-4 my-2">
        <center><h5 class="text-white pt-1 pb-1">Software Developer (Backend)</h5></center>
          <div class="row align-items-center  pb-3">
            <div class="col-auto bg-white rounded">
              <a href="{{ route('sdBList') }}" ><button class="btn btn-info mt-2 mb-2 ">View</button></a>
            </div>
            <div class="col text-white py-2 pl-3 border rounded  shadow-lg ml-3 ">
              <h6>Number of Applicants: {{$total8}}</h6>
              <h6>Pending: {{$pending8}}</h6>
              <h6>Accepted: {{$accept8}}</h6>
              <h6>Rejected: {{$reject8}}</h6>
            </div>
          </div>
        </div>       
      </div>
    </div>
    
 </div>

 <div class="row" >
   
  <div class="col-xl-4 col-md-6">
      <div class="card social-card shadow-lg " style="background-color:#01A9AC;">
        <div class="card-block mx-4 my-2">
        <center><h5 class="text-white pt-1 pb-1">	Mentor (Management)</h5></center>
          <div class="row align-items-center  pb-3">
            <div class="col-auto bg-white rounded">
              <a href="{{ route('mmList') }}" ><button class="btn btn-info mt-2 mb-2 ">View</button></a>
            </div>
            <div class="col text-white py-2 pl-3 border rounded  shadow-lg ml-3 ">
              <h6>Number of Applicants: {{$total9}}</h6>
              <h6>Pending: {{$pending9}}</h6>
              <h6>Accepted: {{$accept9}}</h6>
              <h6>Rejected: {{$reject9}}</h6>
            </div>
          </div>
        </div>       
      </div>
    </div>

    <div class="col-xl-4 col-md-6">
      <div class="card social-card shadow-lg " style="background-color:#01A9AC;">
        <div class="card-block mx-4 my-2">
        <center><h5 class="text-white pt-1 pb-1">Mentor (Technical)</h5></center>
          <div class="row align-items-center  pb-3">
            <div class="col-auto bg-white rounded">
              <a href="{{ route('mtList') }}" ><button class="btn btn-info mt-2 mb-2 ">View</button></a>
            </div>
            <div class="col text-white py-2 pl-3 border rounded  shadow-lg ml-3 ">
              <h6>Number of Applicants: {{$total10}}</h6>
              <h6>Pending: {{$pending10}}</h6>
              <h6>Accepted: {{$accept10}}</h6>
              <h6>Rejected: {{$reject10}}</h6>
            </div>
          </div>
        </div>       
      </div>
    </div>

    <div class="col-xl-4 col-md-6">
      <div class="card social-card shadow-lg " style="background-color:#01A9AC;">
        <div class="card-block mx-4 my-2">
        <center><h5 class="text-white pt-1 pb-1">Co-Ordinator</h5></center>
          <div class="row align-items-center  pb-3">
            <div class="col-auto bg-white rounded">
              <a href="{{ route('coList') }}"><button class="btn btn-info mt-2 mb-2 ">View</button></a>
            </div>
            <div class="col text-white py-2 pl-3 border rounded  shadow-lg ml-3 ">
              <h6>Number of Applicants: {{$total11}}</h6>
              <h6>Pending: {{$pending11}}</h6>
              <h6>Accepted: {{$accept11}}</h6>
              <h6>Rejected: {{$reject11}}</h6>
            </div>
          </div>
        </div>       
      </div>
    </div>

    <div class="col-xl-4 col-md-6">
      <div class="card social-card shadow-lg " style="background-color:#01A9AC;">
        <div class="card-block mx-4 my-2">
        <center><h5 class="text-white pt-1 pb-1">Deleted Records</h5></center>
          <div class="row align-items-center  pb-3">
            <div class="col-auto bg-white rounded">
              <a href="{{ route('applicantListDelete') }}"><button class="btn btn-info mt-2 mb-2 ">View</button></a>
            </div>
            <div class="col text-white py-2 pl-3 border rounded  shadow-lg ml-3 ">
              <h6>Number of Applicants: {{$totalDelete}}</h6>
              <h6>Pending: {{$pendingDelete}}</h6>
              <h6>Accepted: {{$acceptDelete}}</h6>
              <h6>Rejected: {{$rejectDelete}}</h6>
            </div>
          </div>
        </div>       
      </div>
    </div>
    
 </div>
 



@stop