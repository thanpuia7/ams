
@extends('adminlte::page')
@section('title','Applicant Profile')

@section('content')
<div >
    <div  id='DivIdToPrintC' class="position-relative" style =" ">
    
                <div  style ="background-color: rgba(255, 255, 255);" class=" col-xs-12 col-md-12 container rounded border shadow my-md-4 ">


                    <div  class="needs-validation mx-sm-4 mt-3 mt-sm-5 mb-5" >
                        

                            <center>                     
                                <h3 class="media-heading">{{$applicant->name}}</h3>
                                <p><label>Registration No : {{$applicant->registrationNo}} </label></p>
                            </center>
                            <hr>
                            @if($applicant->jobapplys == 1)
                                <center><label>Post Applied :  Project Manager </center>
                            @endif
                            @if($applicant->jobapplys == 2)
                                <center><label>Post Applied :  Senior Management Consultant </center>
                            @endif
                            @if($applicant->jobapplys == 3)
                                <center><label>Post Applied :  Database Administrator </center>
                            @endif
                            @if($applicant->jobapplys == 4)
                                <center><label>Post Applied :  System Administrator </center>
                            @endif
                            @if($applicant->jobapplys == 5)
                                <center><label>Post Applied :  Network & Security Administrator </center>
                            @endif
                            @if($applicant->jobapplys == 6)
                                <center><label>Post Applied :  UI/UX Designer </center>
                            @endif
                            @if($applicant->jobapplys == 7)
                                <center><label>Post Applied :  Software Developer (Frontend) </center>
                            @endif
                            @if($applicant->jobapplys == 8)
                                <center><label>Post Applied :  Software Developer (Backend) </center>
                            @endif
                            @if($applicant->jobapplys == 9)
                                <center><label>Post Applied :  Mentor (Management </center>
                            @endif
                            @if($applicant->jobapplys == 10)
                                <center><label>Post Applied :  Mentor (Technical) </center>
                            @endif
                            @if($applicant->jobapplys == 11)
                                <center><label>Post Applied :  Co-Ordinator </center>
                            @endif

                        </div>
                        <hr>
                    

             
                        <br>
                        <label>Class X Passing Certificate</label>
                        <img src="{{ asset('storage/backend/passingcertificate1/'.$applicant->passingcertificate1URL) }}"  style="width:100%; height:auto;">
                        
                        <br>
                        <label>Class X Marksheet</label>
                        <img src="{{ asset('storage/backend/marksheet1/'.$applicant->marksheet1URL) }}"  style="width:100%; height:auto;">

                        <br>
                        <br>
                        <label>Class XII/Diploma Passing Certificate</label>
                        <img src="{{ asset('storage/backend/passingcertificate2/'.$applicant->passingcertificate2URL) }}"  style="width:100%; height:auto;">

                        <br>
                        <br>
                        <label>Class XII/Diploma Marksheet</label>
                        <img src="{{ asset('storage/backend/marksheet2/'.$applicant->marksheet2URL) }}"  style="width:100%; height:auto;">


                        <br>
                        <br>
                        <label>Bachelor Degree Certificate</label>
                        <img src="{{ asset('storage/backend/passingcertificate3/'.$applicant->passingcertificate3URL) }}"  style="width:100%; height:auto;">

                        <br>
                        <br>
                        <label>Bachelor marksheet</label>
                        <img src="{{ asset('storage/backend/marksheet3/'.$applicant->marksheet3URL) }}"  style="width:100%; height:auto;">


                        @if($applicant->passingcertificate4URL != null)
                        <br>
                        <br>
                        <label>Master Degree Certificate</label>
                        <img src="{{ asset('storage/backend/passingcertificate4/'.$applicant->passingcertificate4URL) }}"  style="width:100%; height:auto;">
                        @endif

                        @if($applicant->marsheet4URL !=null)
                        <br>
                        <br>
                        <label>Master marksheet</label>
                        <img src="{{ asset('storage/backend/marksheet4/'.$applicant->marksheet4URL) }}"  style="width:100%; height:auto;">
                        @endif

                        
                        @if($applicant->experiencecertificate1URL != null)
                        <br>
                        <br>
                        <label>Experience Certificate 1</label>
                        <img src="{{ asset('storage/backend/experiencecertificate1/'.$applicant->experiencecertificate1URL) }}"  style="width:100%; height:auto;">
                        @endif

                        @if($applicant->experiencecertificate2URL !=null)
                        <br>
                        <br>
                        <label>Experience Certificate 2</label>
                        <img src="{{ asset('storage/backend/experiencecertificate2/'.$applicant->experiencecertificate2URL) }}"  style="width:100%; height:auto;">
                        @endif

                        @if($applicant->experiencecertificate3URL != null)
                        <br>
                        <br>
                        <label>Experience Certificate 3</label>
                        <img src="{{ asset('storage/backend/experiencecertificate3/'.$applicant->experiencecertificate3URL) }}"  style="width:100%; height:auto;">
                        @endif

                        @if($applicant->experiencecertificate4URL !=null)
                        <br>
                        <br>
                        <label>Experience Certificate 4</label>
                        <img src="{{ asset('storage/backend/experiencecertificate4/'.$applicant->experiencecertificate4URL) }}"  style="width:100%; height:auto;">
                        @endif

                        @if($applicant->experiencecertificate5URL !=null)
                        <br>
                        <br>
                        <label>Experience Certificate 5</label>
                        <img src="{{ asset('storage/backend/experiencecertificate5/'.$applicant->experiencecertificate5URL) }}"  style="width:100%; height:auto;">
                        @endif

                        <div class="modal-footer">
                            <center>
                                <input type='button' id='btn' value='Print' onclick='printDiv();'>
                            </center>
                        </div>

                    </div>

                </div>
    </div>
</div>
<!-- <div>
            <p class="py-3 text-xs font-weight-light text-center">Crafted with care by <a href="#" class="text-red">Mizoram State e-Governance Society (MSeGS)</a>, hosted by department of ICT, Government of Mizoram</p>
</div> -->


@stop

 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> 


 <script type='text/javascript'>

function printDiv() 
{

  var divToPrint=document.getElementById('DivIdToPrintC');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
</script>

<script src="/js/print.js"></script>

</body>
</html>

