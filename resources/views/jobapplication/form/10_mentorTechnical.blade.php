@extends('layouts/app')
@section('title','Home')

@section('content')
<div style ="background-color: rgba(241, 241, 241);" class="position-relative">

    <!-- <div class="d-none d-lg-block "> -->
    <div class=" ">
        <div style="height: 200px; background-color: rgba(73, 114, 222);" class=" ">   
            <CENTER> 
                <div class="container">
                    <h4 class="text-white pt-4 pt-md-5 ">APPLICATION FOR EMPLOYMENT</H4>
                    <br>
                </div>
            </center>
        </div>
    </div>

<!-- <div style ="background-color: rgba(13, 56, 130);" class=" d-lg-none">

<p class="text-white px-3 py-3">He portal hi Mizoram mi leh sa dik tak, Mizoram pawna Covid-19 hrileng vanga tangkhang ten, Mizoram Sawrkar hnena an in report theihna tura buatsaih a ni e (Mizoram chhunga awm mek te tan a ni lo! )</p>

</div> -->

    <div class="position-relative">
        <div class="position-absolute" style="left:0; right:0; top: -100px;">
            <div  style ="background-color: rgba(255, 255, 255);" class=" col-xs-12 col-md-10 container rounded border shadow my-md-4 ">

                @if (Session::has('userexist'))
                <div class="alert alert-info">
                    <ul>
                        <li>{{ Session::get('userexist') }}</li>
                    </ul>
                </div>
                @endif

                <form  class="needs-validation mx-sm-4 mt-3 mt-sm-5 mb-5" method="POST" action="{{ route('createbackendDeveloper') }}" enctype="multipart/form-data">
                @csrf

                    <div  class="mt-1 mb-3 border py-2">
                            
                            <center><label><b>MENTOR (TECHNICAL)</b></label></center>
                    </div>


                    <div  class="form-row ">
                        <div class="col-md-6 mb-2 pr-2">
                            <label  for="name">1. Name of Applicant</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="" >
                        <p class=" text-danger">{{$errors->first('name')}}</p>
                        </div>
                        <div class="col-md-6 mb-2 pl-2">
                            <label for="passport">2. Passport Photo</label>
                            <input type="file" name="passport" class="form-control py-1" id="passport" placeholder="" >
                        <p class=" text-danger">{{$errors->first('passport')}}</p>
                        </div>

                    </div>

                    
                    <div  class="form-row ">
                        <div class="col-md-6 mb-2 pr-2">
                            <label  for="name">3. Email</label>
                            <input type="email" name="email" class="form-control" id="email" placeholder="" >
                        <p class=" text-danger">{{$errors->first('email')}}</p>
                        </div>
                        <div class="col-md-6 mb-2 pl-2">
                            <label for="gender">4. Gender *</label>
                            <select name ="gender" class="form-control">

                                <option value="" disabled selected>Select</option>
                            
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                <option value="Other">Other</option>
                                
                            </select>

                            <p class="text-danger">{{$errors->first('gender')}}</p>
                        </div>

                    </div>

                    <div class="form-row" >
                        <div class="col-md-6 mb-2 pr-2">
                            <label for="phone">5. Mobile Number(For SMS) *</label>
                            <input type="text" name="phone" class="form-control" id="phone" placeholder="" >
                            <p class=" text-danger">{{$errors->first('phone')}}</p>
                        </div>

                        <div class="col-md-6 mb-2 pr-2">
                            <label for="whatsapp">6. WhatsApp Number*</label>
                            <input type="text" name="whatsapp" class="form-control" id="whatsapp" placeholder="" >
                            <p class=" text-danger">{{$errors->first('whatsapp')}}</p>
                        </div>

                        

                    </div>


                    <div  class="form-row ">
                        <div class="col-md-6 mb-2 pl-2">
                            <label for="religion">7. Religion *</label>
                            <select name ="religion" id="religion" class="form-control" >
                                <option value="" disabled selected>Select </option>
                        
                                <option value="Christian">Christian</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Bhuddhist">Bhuddhist</option>
                                <option value="Muslim">Muslim</option>
                                <option value="Other">Other</option>

                                
                            </select>

                            <p class=" text-danger">{{$errors->first('religion')}}</p>
                        </div>
                        <div class="col-md-6 mb-2 pl-2">
                            <label for="name">8. Date of Birth</label>
                            <input type="date" name="dob" class="form-control" id="dob" placeholder="" >
                        <p class=" text-danger">{{$errors->first('dob')}}</p>
                        </div>

                    </div>

                    <div  class="form-row ">
                        <div class="col-md-6 mb-2 pr-2">
                            <label for="name">9. Father's or Mother's Name</label>
                            <input type="text" name="fathersname" class="form-control" id="fathersname" placeholder="" >
                        <p class=" text-danger">{{$errors->first('fathersname')}}</p>
                        </div>
                        <div class="col-md-6 mb-2 pl-2">
                            <label for="caste">10. Caste *</label>
                            <select name ="caste" id="caste" class="form-control" >
                                        <option value="" disabled selected>Select </option>
                                
                                        <option value="Scheduled Tribe">Scheduled Tribe</option>
                                        <option value="Scheduled Caste">Scheduled Caste</option>
                                        <option value="OBC">OBC</option>
                                        <option value="General">General</option>
                                
                                </select>

                                <p class=" text-danger">{{$errors->first('caste')}}</p>
                        </div>

                    </div>

                    <div  class="form-row ">
                        <div class="col-md-6 mb-2 pr-2">
                            <label for="name">11. State</label>
                            <input type="text" name="state" class="form-control" id="state" placeholder="" >
                        <p class=" text-danger">{{$errors->first('state')}}</p>
                        </div>
                        <div class="col-md-6 mb-2 pl-2">
                            <label for="district">12. District *</label>
                            <select name ="district" id="district" class="form-control" >
                                <option value="" disabled selected>Select </option>
                        
                                <option value="Aizawl">Aizawl</option>
                                <option value="Lunglei">Lunglei</option>
                                <option value="Champhai">Champhai</option>
                                <option value="Lawngtlai">Lawngtlai</option>
                                <option value="Mamit">Mamit</option>
                                <option value="Siaha">Siaha</option>
                                <option value="Kolasib">Kolasib</option>
                                <option value="Serchhip">Serchhip</option>
                                <option value="Saitual">Saitual</option>
                                <option value="Khawzawl">Khawzawl</option>
                                <option value="Hnahthial">Hnahthial</option>
                                <option value="Other">Other</option>

                                
                            </select>

                            <p class=" text-danger">{{$errors->first('district')}}</p>
                        </div>

                    </div>

                    
                    <div  class="form-row ">
                        <!-- <div class="col-md-6 mb-2 pr-2">
                            <label for="name">13. Permanent Address </label>
                            <textarea  name="paddress" class="form-control" rows="3" id="paddress" placeholder="" ></textarea>
                        <p class=" text-danger">{{$errors->first('paddress')}}</p>
                        </div> -->
                        <div class="col-md-6 mb-2 pl-2">
                            <label for="name">13. Correspondence Address</label>
                            <textarea name="caddress" class="form-control" rows="3" id="caddress" placeholder="" ></textarea>
                        <p class=" text-danger">{{$errors->first('caddress')}}</p>
                        </div>

                    </div>

                    <br>
                    

                    <p >14. Educational Qualification:</p>

                    <div class = "border pl-2 pr-2 pt-2  mb-2">
                        <table class="table table-responsive ">
                            <thead style="width: 2000px !important;">
                                <tr>
                                    <td class =" " scope="col"></td>
                                    <td class = "text-muted " scope="col">Examination</td>
                                    <td class = "text-muted " scope="col">Subject/Branch</td>
                                    <td class = "text-muted " scope="col">School/University/College</td>
                                    <td class = "text-muted " scope="col">Year of Passing</td>
                                    <td class = "text-muted " scope="col">Marksheet</td>
                                    <td class = "text-muted " scope="col">Passing Certificate</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class = "text-muted" scope="row">1</td>
                                    <td>
                                        <p>Class X</p>
                                    </td>
                                    <td>
                                        <input type="text" name="subject1" class="form-control" id="subject1" placeholder="" disabled>
                                        <p class=" text-danger">{{$errors->first('subject1')}}</p>
                                       
                                    </td>

                                    <td>
                                        <input type="text" name="university1" class="form-control" id="university1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('university1')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="year1" class="form-control" id="year1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('year1')}}</p>
                                    </td>

                                    <td>
                                        <input type="file" name="marksheet1" class="border py-1 px-1" id="marksheet1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('marksheet1')}}</p>
                                    </td>
                                    <td>
                                        <input type="file" name="passingcertificate1" class="border py-1 px-1" id="passingcertificate1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('passingcertificate1')}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class = "text-muted" scope="row">2</td>
                                    <td>
                                        <p>Class XII/Diploma</p>
                                    </td>
                                    <td>
                                        <input type="text" name="subject2" class="form-control" id="subject2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('subject2')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="university2" class="form-control" id="university2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('university2')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="year2" class="form-control" id="year2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('year2')}}</p>
                                    </td>

                                    <td>
                                        <input type="file" name="marksheet2" class="form-control py-1" id="marksheet2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('marksheet2')}}</p>
                                    </td>
                                    <td>
                                        <input type="file" name="passingcertificate2" class="form-control py-1" id="passingcertificate2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('passingcertificate2')}}</p>
                                    </td>

                                </tr>

                                <tr>
                                    <td class = "text-muted" scope="row">3</td>
                                    <td>
                                        <p>Bachelor Degree</p>
                                    </td>
                                    <td>
                                        <input type="text" name="subject3" class="form-control" id="subject3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('subject3')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="university3" class="form-control" id="university3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('university3')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="year3" class="form-control" id="year3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('year3')}}</p>
                                    </td>

                                    <td>
                                        <input type="file" name="marksheet3" class="form-control py-1" id="marksheet3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('marksheet3')}}</p>
                                    </td>

                                    <td>
                                        <input type="file" name="passingcertificate3" class="form-control py-1" id="passingcertificate3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('passingcertificate3')}}</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td class = "text-muted" scope="row">4</td>
                                    <td>
                                        <p>Master Degree</p>
                                    </td>
                                    <td>
                                        <input type="text" name="subject4" class="form-control" id="subject4" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('subject4')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="university4" class="form-control" id="university4" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('university4')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="year4" class="form-control" id="year4" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('year4')}}</p>
                                    </td>

                                    <td>
                                        <input type="file" name="marksheet4" class="form-control py-1" id="marksheet4" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('marksheet4')}}</p>
                                    </td>
                                    <td>
                                        <input type="file" name="passingcertificate4" class="form-control py-1" id="passingcertificate4" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('passingcertificate4')}}</p>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>


                    <br>
                    <p >15. Professional Course Attended:</p>

                    <div class = "border pl-2 pr-2 pt-2  mb-2">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <td scope="col"></td>
                                    <td class = "text-muted" scope="col">Course Name</td>
                                    <td class = "text-muted" scope="col">Period</td>
                                    <td class = "text-muted" scope="col">Institute</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class = "text-muted" scope="row">1</td>

                                    <td>
                                        <input type="text" name="coursename1" class="form-control" id="coursename1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('coursename1')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="period1" class="form-control" id="period1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('period1')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="institute1" class="form-control" id="institute1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('institute1')}}</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td class = "text-muted" scope="row">2</td>
                                    <td>
                                        <input type="text" name="coursename1" class="form-control" id="coursename1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('coursename1')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="period2" class="form-control" id="period2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('period2')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="institute2" class="form-control" id="institute2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('institute2')}}</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td class = "text-muted" scope="row">3</td>
                                    <td>
                                        <input type="text" name="coursename3" class="form-control" id="coursename3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('coursename3')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="period3" class="form-control" id="period3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('period3')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="institute3" class="form-control" id="institute3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('institute3')}}</p>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>


                    <br>
                    <p >16. Work Experience:</p>

                    <div class = "border pl-2 pr-2 pt-2  mb-2">
                        <table class="table table-borderless table-responsive " >
                            <thead style="">
                                <tr >
                                    <td class = "text-muted " scope="col">Post Name</td>
                                    <td  class = "text-muted " scope="col">Organization Name</td>
                                    <td  class = "text-muted " scope="col">Duration</td>
                                    <td class = "text-muted " scope="col">Experience Certificate</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="text" name="postname1" class="form-control " id="postname1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('postname1')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="organizationname1" class="form-control " id="organizationname1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('organizationname1')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="duration1" class="form-control " id="duration1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('duration1')}}</p>
                                    </td>
                                    <td>
                                        <input type="file" name="experiencecertificate1" class="form-control px-1 py-1 " id="experiencecertificate1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('experiencecertificate1')}}</p>
                                    </td>


                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" name="postname2" class="form-control w-auto" id="postname2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('postname2')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="organizationname2" class="form-control w-auto" id="organizationname2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('organizationname2')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="duration2" class="form-control w-auto" id="duration2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('duration2')}}</p>
                                    </td>
                                    <td>
                                        <input type="file" name="experiencecertificate2" class="form-control px-1 py-1 w-auto" id="experiencecertificate2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('experiencecertificate2')}}</p>
                                    </td>


                                </tr>

                                <tr>
                                    <td>
                                        <input type="text" name="postname3" class="form-control w-auto" id="postname3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('postname3')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="organizationname3" class="form-control w-auto" id="organizationname3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('organizationname3')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="duration3" class="form-control w-auto" id="duration3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('duration3')}}</p>
                                    </td>
                                    <td>
                                        <input type="file" name="experiencecertificate3" class="form-control px-1 py-1 w-auto" id="experiencecertificate3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('experiencecertificate3')}}</p>
                                    </td>


                                </tr>

                                <tr>
                                    <td>
                                        <input type="text" name="postname4" class="form-control w-auto" id="postname4" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('postname4')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="organizationname4" class="form-control w-auto" id="organizationname4" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('organizationname4')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="duration4" class="form-control w-auto" id="duration4" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('duration4')}}</p>
                                    </td>
                                    <td>
                                        <input type="file" name="experiencecertificate4" class="form-control px-1 py-1 w-auto" id="experiencecertificate4" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('experiencecertificate4')}}</p>
                                    </td>


                                </tr>

                                <tr>
                                    <td>
                                        <input type="text" name="postname5" class="form-control w-auto" id="postname5" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('postname5')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="organizationname5" class="form-control w-auto" id="organizationname5" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('organizationname5')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="duration5" class="form-control w-auto" id="duration5" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('duration5')}}</p>
                                    </td>
                                    <td>
                                        <input type="file" name="experiencecertificate5" class="form-control px-1 py-1 w-auto" id="experiencecertificate5" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('experiencecertificate5')}}</p>
                                    </td>


                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <br>
                    

                    <br>
                    <p >17. Technical Knowledge : (Please Tick if Any)</p>

                    <div class = "border pl-2 pr-2 pt-3  mb-2">
                        <div class="row">
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value ="Linux OS" class="mr-2">Linux OS
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value = "Windows Server " class="mr-2">Windows Server 
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value = "CMS" class="mr-2">CMS                          
                            </div> 

                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value ="HTML" class="mr-2">HTML
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value = "CSS" class="mr-2">CSS                        
                            </div>   
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value ="JavaScript" class="mr-2">JavaScript
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value ="CMS(Wordpress)" class="mr-2">CMS(Wordpress)
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value = "Php" class="mr-2">Php                         
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value ="Core Jave" class="mr-2">Core Java
                            </div>
                        </div>

                        <div class="row ">
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value ="VueJS" class="mr-2">VueJs
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value = "Angular" class="mr-2">Angular                           
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value ="nodeJs" class="mr-2">nodeJs
                            </div>
                        </div>

                        <div class="row ">
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value ="Laravel" class="mr-2">Laravel
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value = "Spring MVC" class="mr-2">Spring MVC                           
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value ="Bootstrap" class="mr-2">Bootstrap
                            </div>
                        </div>

                        <div class="row ">
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value ="React" class="mr-2">React
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value = "JBoss" class="mr-2">JBoss                          
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value ="Tomcat" class="mr-2">Tomcat
                            </div>
                        </div>

                        <div class="row ">
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value ="MySql" class="mr-2">MySql
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value = "NoSQL" class="mr-2">NoSQL                          
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value ="MongoDb" class="mr-2">MongoDb
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value ="SMS Integration" class="mr-2">SMS Integration
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value = "Single Sign-on" class="mr-2">Single Sign-on                          
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value ="Payment Gateway Integration" class="mr-2">Payment Gateway Integration
                            </div>
                        </div>

                        <div class="row ">
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value ="Arduino" class="mr-2">Arduino
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value = "IoT" class="mr-2">IoT                         
                            </div>
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value ="Jira" class="mr-2">Jira
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-4">
                                <input type="checkbox" name="technicalknowledge[]" value ="git" class="mr-2">git
                            </div>
                            <div class="col-md-8">
                                <input type="checkbox" name="technicalknowledge[]" value ="Software Project Management - MS Project 2013" class="mr-2">Software Project Management - MS Project 2013
                            </div>
     
                        </div>

                        <p class=" text-danger">{{$errors->first('technicalknowledge')}}</p>

                    </div>

                    <br>
                    <p  for="portfolio">18. Upload Project Porfolio (in pdf):</p>

                    <div class = "border pl-2 pr-2 pt-2  mb-2">
                        <div class="row ml-2">
                            <div class="col-md-6  my-3 py-3 pr-2 image-upload">
                                
                                <input type="file" name="portfolio" class="col-10 border py-1 px-1">
                                <p class=" text-danger">{{$errors->first('portfolio')}}</p>
                            </div> 

                            <!-- <div class="col-md-6 mt-5 py-2 pr-2 image-upload"> 
                                <label for="file-input" >
                                    <img src="/image/profileimage.png" class="border rounded px-2 py-2"  style="border-style:dotted;width:20%; height: 20%; padding:0px" >
                                    <p>Upload your passport photo(max size 900Kb)</p>
                                </label>
                                <input id="file-input" type="file" name="image" style="display:none;" />
                            </div> -->
                        </div>

                    </div>
                       
                    <div>
                        <label for="vehicle1" class="pb-3 ">
                            <input type="checkbox" name="agree" value="1">
                             I certify that the foregoing information is correct and complete to the best of my knowledge and belief.
                             <p class=" text-danger">{{$errors->first('agree')}}</p>
                        </label><br>
                    </div>
  
                    <div class="col-md-6">
                        <label class="ohnohoney" for="email1"></label>
                        <input class="ohnohoney" autocomplete="off" type="email" id="email1" name="email1" placeholder="Your e-mail here">
                    </div>

                    <div class="form-group">
    		            <input type="hidden" name="jobapply" value="{{ $applyid}}">
	                </div>

                    <button class="btn btn-primary" type="submit">Submit form</button>

                </form>

            </div>

            <hr>



            <div>
                    <p class="py-3 text-xs font-weight-light text-center">Crafted with care by <a href="#" class="text-red">Mizoram State e-Governance Society (MSeGS)</a>, hosted by department of ICT, Government of Mizoram</p>
            </div>



        </div>
    </div>
 

</div>
@endsection