

@extends('adminlte::page')
@section('title','Applicant List')

@section('content')
	
<div class="card">
  <div class="card-header">
    <h3 class="card-title">List of Applicant</h3>
  </div>
              <!-- /.card-header -->
  <div class="table-responsive card-body w-full col-md-12 text-nowrap">
    <table id="example2" class="table table-striped table-hover">
      <thead>
        <tr>
          <th>Applicant Name</th>
          <th>Email</th>
          <th>Phone Number</th>
          <th>Gender</th>
          <th class="d-flex justify-content-center">Print</th>
        </tr>
      </thead>
      
      <tbody>  
        @foreach($Backenddeveloper as $applicant)
        <tr>
          <td>{{$applicant->name}}</td>
          <td>{{$applicant->email}}</td>
          <td>{{$applicant->gender}}</td>
          <td>{{$applicant->dob}}</td>
          <td class="d-flex justify-content-center"><a href="{{ route('applicantProfile',$applicant->id) }}" class="btn btn-success btn-xs"><i class="fa fa-print" aria-hidden="true" data-content="View"></i></a></td>
        </tr>

       @endforeach              
      </tbody>
                  <tfoot>
                  <tr>
                    <th>Total: {{$total}}</th>
                    <th>Accepted: {{$accept}}</th>
                    <th>Rejected: {{$reject}}</th>
                    <th>Pending: {{$pending}}</th>

                  </tr>
                  </tfoot>
    </table>
  </div>
              <!-- /.card-body -->
  <div class="paginator container">
        {{ $Backenddeveloper->appends($_GET)->onEachSide(1)->links() }}
  </div>

</div>

  @stop
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
	<script>
		$('.js-pscroll').each(function(){
			var ps = new PerfectScrollbar(this);

			$(window).on('resize', function(){
				ps.update();
			})

			$(this).on('ps-x-reach-start', function(){
				$(this).parent().find('.table100-firstcol').removeClass('shadow-table100-firstcol');
			});

			$(this).on('ps-scroll-x', function(){
				$(this).parent().find('.table100-firstcol').addClass('shadow-table100-firstcol');
			});

		});

		
		
		
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>