@extends('layouts/app')
@section('title','Home')

@section('content')
<div style ="background-color: rgba(241, 241, 241);" class="position-relative">

    <!-- <div class="d-none d-lg-block "> -->
    <div class=" ">
        <div style="height: 200px; background-color: rgba(73, 114, 222);" class=" ">   
            <CENTER> 
                <div class="container">
                    <h4 class="text-white pt-5">APPLICATION FOR EMPLOYMENT</H4>
                    <br>
                </div>
            </center>
        </div>
    </div>

<!-- <div style ="background-color: rgba(13, 56, 130);" class=" d-lg-none">

<p class="text-white px-3 py-3">He portal hi Mizoram mi leh sa dik tak, Mizoram pawna Covid-19 hrileng vanga tangkhang ten, Mizoram Sawrkar hnena an in report theihna tura buatsaih a ni e (Mizoram chhunga awm mek te tan a ni lo! )</p>

</div> -->

    <div class="position-relative">
        <div class="position-absolute" style="left:0; right:0; top: -100px;">
            <div  style ="background-color: rgba(255, 255, 255);" class=" col-xs-12 col-md-8 container rounded border shadow my-md-4 ">

                @if (Session::has('userexist'))
                <div class="alert alert-info">
                    <ul>
                        <li>{{ Session::get('userexist') }}</li>
                    </ul>
                </div>
                @endif

                <form  class="needs-validation mx-sm-4 mt-3 mt-sm-5 mb-5" method="POST" action="{{ route('createJob') }}" enctype="multipart/form-data">
                @csrf

                    <div  class="form-row ">
                        <div class="col-md-6 mb-2 pr-2">
                            <label  for="name">Name of Applicant</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="" >
                        <p class=" text-danger">{{$errors->first('name')}}</p>
                        </div>
                        <div class="col-md-6 mb-2 pl-2">
                            <label for="name">Email</label>
                            <input type="email" name="email" class="form-control" id="email" placeholder="" >
                        <p class=" text-danger">{{$errors->first('email')}}</p>
                        </div>

                    </div>

                    <div class="form-row" >
                        <div class="col-md-6 mb-2 pr-2">
                            <label for="phone">Contact Number *</label>
                            <input type="text" name="phone" class="form-control" id="phone" placeholder="" >
                            <p class=" text-danger">{{$errors->first('phone')}}</p>
                        </div>

                        
                        <div class="col-md-6 mb-2 pl-2">
                            <label for="gender">Gender *</label>
                            <select name ="gender" class="form-control">

                                <option value="" disabled selected>Select</option>
                            
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                <option value="Other">Other</option>
                                
                            </select>

                            <p class="text-danger">{{$errors->first('gender')}}</p>
                        </div>
                    </div>


                    <div  class="form-row ">
                        <div class="col-md-6 mb-2 pl-2">
                            <label for="religion">Religion *</label>
                            <select name ="religion" id="religion" class="form-control" >
                                <option value="" disabled selected>Select </option>
                        
                                <option value="Christian">Christian</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Bhuddhist">Bhuddhist</option>
                                <option value="Muslim">Muslim</option>
                                <option value="Other">Other</option>

                                
                            </select>

                            <p class=" text-danger">{{$errors->first('religion')}}</p>
                        </div>
                        <div class="col-md-6 mb-2 pl-2">
                            <label for="name">Date of Birth</label>
                            <input type="date" name="dob" class="form-control" id="dob" placeholder="" >
                        <p class=" text-danger">{{$errors->first('dob')}}</p>
                        </div>

                    </div>

                    <div  class="form-row ">
                        <div class="col-md-6 mb-2 pr-2">
                            <label for="name">Father's or Mother's Name</label>
                            <input type="text" name="fathersname" class="form-control" id="fathersname" placeholder="" >
                        <p class=" text-danger">{{$errors->first('fathersname')}}</p>
                        </div>
                        <div class="col-md-6 mb-2 pl-2">
                            <label for="caste">Caste *</label>
                            <select name ="caste" id="caste" class="form-control" >
                                        <option value="" disabled selected>Select </option>
                                
                                        <option value="Scheduled Tribe">Scheduled Tribe</option>
                                        <option value="Scheduled Caste">Scheduled Caste</option>
                                        <option value="OBC">OBC</option>
                                        <option value="General">General</option>
                                
                                </select>

                                <p class=" text-danger">{{$errors->first('caste')}}</p>
                        </div>

                    </div>

                    <div  class="form-row ">
                        <div class="col-md-6 mb-2 pr-2">
                            <label for="name">State</label>
                            <input type="text" name="state" class="form-control" id="state" placeholder="" >
                        <p class=" text-danger">{{$errors->first('state')}}</p>
                        </div>
                        <div class="col-md-6 mb-2 pl-2">
                            <label for="district">District *</label>
                            <select name ="district" id="district" class="form-control" >
                                <option value="" disabled selected>Select </option>
                        
                                <option value="Aizawl">Aizawl</option>
                                <option value="Lunglei">Lunglei</option>
                                <option value="Champhai">Champhai</option>
                                <option value="Lawngtlai">Lawngtlai</option>
                                <option value="Mamit">Mamit</option>
                                <option value="Siaha">Siaha</option>
                                <option value="Kolasib">Kolasib</option>
                                <option value="Serchhip">Serchhip</option>
                                <option value="Saitual">Saitual</option>
                                <option value="Khawzawl">Khawzawl</option>
                                <option value="Hnahthial">Hnahthial</option>
                                <option value="Other">Other</option>

                                
                            </select>

                            <p class=" text-danger">{{$errors->first('district')}}</p>
                        </div>

                    </div>

                    
                    <div  class="form-row ">
                        <div class="col-md-6 mb-2 pr-2">
                            <label for="name">Permanent Address </label>
                            <textarea  name="paddress" class="form-control" rows="3" id="paddress" placeholder="" ></textarea>
                        <p class=" text-danger">{{$errors->first('paddress')}}</p>
                        </div>
                        <div class="col-md-6 mb-2 pl-2">
                            <label for="name">Correspondence Address</label>
                            <textarea name="caddress" class="form-control" rows="3" id="caddress" placeholder="" ></textarea>
                        <p class=" text-danger">{{$errors->first('caddress')}}</p>
                        </div>

                    </div>

                    <br>
                    

                    <p >Educational Qualification:</p>

                    <div class = "border pl-2 pr-2 pt-2  mb-2">
                        <table class="table table-borderless table-responsive">
                            <thead>
                                <tr>
                                <td scope="col"></td>
                                <td class = "text-muted" scope="col">Examination</td>
                                <td class = "text-muted" scope="col">University</td>
                                <td class = "text-muted" scope="col">Percentage</td>
                                <td class = "text-muted" scope="col">Subjects</td>
                                <td class = "text-muted" scope="col">Year</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class = "text-muted" scope="row">1</td>
                                    <td>
                                        <input type="text" name="examination1" class="form-control" id="examination1" placeholder="Class X" >
                                        <p class=" text-danger">{{$errors->first('examination1')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="university1" class="form-control" id="university1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('university1')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="percentage1" class="form-control" id="percentage1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('percentage1')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="subject1" class="form-control" id="subject1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('subject1')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="year1" class="form-control" id="year1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('year1')}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class = "text-muted" scope="row">2</td>
                                    <td>
                                        <input type="text" name="examination2" class="form-control" id="examination2" placeholder="Class XII or Diploma" >
                                        <p class=" text-danger">{{$errors->first('examination2')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="university2" class="form-control" id="university2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('university2')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="percentage2" class="form-control" id="percentage2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('percentage2')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="subject2" class="form-control" id="subject2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('subject2')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="year2" class="form-control" id="year2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('year2')}}</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td class = "text-muted" scope="row">3</td>
                                    <td>
                                        <input type="text" name="examination3" class="form-control" id="examination3" placeholder="Bachelor Degree" >
                                        <p class=" text-danger">{{$errors->first('examination3')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="university3" class="form-control" id="university3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('university3')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="percentage3" class="form-control" id="percentage3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('percentage3')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="subject3" class="form-control" id="subject3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('subject3')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="year3" class="form-control" id="year3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('year3')}}</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td class = "text-muted" scope="row">4</td>
                                    <td>
                                        <input type="text" name="examination4" class="form-control" id="examination4" placeholder="Master or Other" >
                                        <p class=" text-danger">{{$errors->first('examination4')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="university4" class="form-control" id="university4" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('university4')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="percentage4" class="form-control" id="percentage4" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('percentage4')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="subject4" class="form-control" id="subject4" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('subject4')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="year4" class="form-control" id="year4" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('year4')}}</p>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>


                    <br>
                    <p >Professional Training:</p>

                    <div class = "border pl-2 pr-2 pt-2  mb-2">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <td scope="col"></td>
                                    <td class = "text-muted" scope="col">Name of Training Program</td>
                                    <td class = "text-muted" scope="col">Period</td>
                                    <td class = "text-muted" scope="col">Institute</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class = "text-muted" scope="row">1</td>

                                    <td>
                                        <input type="text" name="nameoftraining1" class="form-control" id="nameoftraining1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('nameoftraining1')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="trainingperiod1" class="form-control" id="trainingperiod1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('trainingperiod1')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="traininginstitute1" class="form-control" id="traininginstitute1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('traininginstitute1')}}</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td class = "text-muted" scope="row">2</td>
                                    <td>
                                        <input type="text" name="nameoftraining2" class="form-control" id="nameoftraining2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('nameoftraining2')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="trainingperiod2" class="form-control" id="trainingperiod2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('trainingperiod2')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="traininginstitute2" class="form-control" id="traininginstitute2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('traininginstitute2')}}</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td class = "text-muted" scope="row">3</td>
                                    <td>
                                        <input type="text" name="nameoftraining3" class="form-control" id="nameoftraining3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('nameoftraining3')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="trainingperiod3" class="form-control" id="trainingperiod3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('trainingperiod3')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="traininginstitute3" class="form-control" id="traininginstitute3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('traininginstitute3')}}</p>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>


                    <br>
                    <p >Work Experience with particular post held:</p>

                    <div class = "border pl-2 pr-2 pt-2  mb-2">
                        <table class="table table-borderless table-responsive " >
                            <thead style="width: 2000px !important;">
                                <tr >
                                    <td class = "text-muted " scope="col">Period</td>
                                    <td  class = "text-muted " scope="col-1">Join Date</td>
                                    <td  class = "text-muted " scope="col">Post Held</td>
                                    <td class = "text-muted " scope="col">Employer</td>
                                    <td class = "text-muted " scope="col-4" >Last Basic Pay</td>
                                    <td class = "text-muted " scope="col">Achievement</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="text" name="expperiod1" class="form-control w-auto" id="expperiod1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('expperiod1')}}</p>
                                    </td>

                                    <td>
                                        <input type="date" name="expjoindate1" class="form-control w-auto" id="expjoindate1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('expjoindate1')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="exppostheld1" class="form-control w-auto" id="exppostheld1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('exppostheld1')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="expemployer1" class="form-control w-auto" id="expemployer1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('expemployer1')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="explastbasic1" class="form-control w-auto" id="explastbasic1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('explastbasic1')}}</p>
                                    </td>
                                    <td>
                                        <input type="textarea" name="expacheivement1" class="form-control w-auto" id="expacheivement1" placeholder="" rows="2">
                                        <p class=" text-danger">{{$errors->first('expacheivement1')}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" name="expperiod2" class="form-control w-auto" id="expperiod2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('expperiod2')}}</p>
                                    </td>

                                    <td>
                                        <input type="date" name="expjoindate2" class="form-control" id="expjoindate2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('expjoindate2')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="exppostheld2" class="form-control" id="exppostheld2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('exppostheld2')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="expemployer2" class="form-control" id="expemployer2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('expemployer2')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="explastbasic2" class="form-control" id="explastbasic2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('explastbasic2')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="expacheivement2" class="form-control" id="expacheivement2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('expacheivement2')}}</p>
                                    </td>

                                </tr>

                                <tr>
                                    <td>
                                        <input type="text" name="expperiod3" class="form-control" id="expperiod3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('expperiod3')}}</p>
                                    </td>

                                    <td>
                                        <input type="date" name="expjoindate3" class="form-control" id="expjoindate3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('expjoindate3')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="exppostheld3" class="form-control" id="exppostheld3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('exppostheld3')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="expemployer3" class="form-control" id="expemployer3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('expemployer3')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="explastbasic3" class="form-control" id="explastbasic3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('explastbasic3')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="expacheivement3" class="form-control" id="expacheivement3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('expacheivement3')}}</p>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>



                    <br>
                    <p >Ay Other Experience, If Any:</p>

                    <div class = "border pl-2 pr-2 pt-2  mb-2">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <td scope="col"></td>
                                    <td class = "text-muted" scope="col">Post Held</td>
                                    <td class = "text-muted" scope="col">Organization</td>
                                    <td class = "text-muted" scope="col">Period</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class = "text-muted" scope="row">1</td>

                                    <td>
                                        <input type="text" name="oexppostheld1" class="form-control" id="oexppostheld1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('oexppostheld1')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="oexporganiation1" class="form-control" id="oexporganiation1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('oexporganiation1')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="oexeperiod1" class="form-control" id="oexeperiod1" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('oexeperiod1')}}</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td class = "text-muted" scope="row">2</td>
                                    <td>
                                        <input type="text" name="oexppostheld2" class="form-control" id="oexppostheld2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('oexppostheld2')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="oexporganiation2" class="form-control" id="oexporganiation2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('oexporganiation2')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="oexeperiod2" class="form-control" id="oexeperiod2" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('oexeperiod2')}}</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td class = "text-muted" scope="row">3</td>
                                    <td>
                                        <input type="text" name="oexppostheld3" class="form-control" id="oexppostheld3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('oexppostheld3')}}</p>
                                    </td>

                                    <td>
                                        <input type="text" name="oexporganiation3" class="form-control" id="oexporganiation3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('oexporganiation3')}}</p>
                                    </td>
                                    <td>
                                        <input type="text" name="oexeperiod3" class="form-control" id="oexeperiod3" placeholder="" >
                                        <p class=" text-danger">{{$errors->first('oexeperiod3')}}</p>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>


                    <br>
                    <p >Technical/Management:</p>

                    <div class = "border pl-2 pr-2 pt-2  mb-2">
                        <table class="table table-borderless table-responsive">
                            <thead>
                                <tr>
                                <td scope="col"></td>
                                <td class = "text-muted" scope="col">Area of Specialization</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr> 
                                    <td class = "text-muted" scope="row">1</td>
                                    <td>
                                        <input type="text" name="areaofspecialization1" class="form-control w-auto" id="areaofspecialization1" placeholder="" size="50">
                                        <p class=" text-danger">{{$errors->first('areaofspecialization1')}}</p>
                                    </td>
                                </tr>

                                <tr> 
                                    <td class = "text-muted" scope="row">2</td>
                                    <td>
                                        <input type="text" name="areaofspecialization2" class="form-control w-auto" id="areaofspecialization2" placeholder="" size="50">
                                        <p class=" text-danger">{{$errors->first('areaofspecialization2')}}</p>
                                    </td>
                                </tr>
                                <tr> 
                                    <td class = "text-muted" scope="row">3</td>
                                    <td>
                                        <input type="text" name="areaofspecialization3" class="form-control w-auto" id="areaofspecialization3" placeholder="" size="50">
                                        <p class=" text-danger">{{$errors->first('areaofspecialization3')}}</p>
                                    </td>
                                </tr>
                                <tr> 
                                    <td class = "text-muted" scope="row">4</td>
                                    <td>
                                        <input type="text" name="areaofspecialization4" class="form-control w-auto" id="areaofspecialization4" placeholder="" size="50">
                                        <p class=" text-danger">{{$errors->first('areaofspecialization4')}}</p>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
<!-- 
                    <div class="col-md-6 py-3 pr-2">
                        <label  for="name">Upload your passport photo:</label>
                        <input type="file" name="image" class="">
                    </div> -->

                    <div class="col-md-6 mt-5 py-2 pr-2 image-upload"> 
                        <label for="file-input" >
                            <img src="/image/profileimage.png" class="border rounded px-2 py-2"  style="border-style:dotted;width:20%; height: 20%; padding:0px" >
                            <p>Upload your passport photo(max size 900Kb)</p>
                        </label>
                        <input id="file-input" type="file" name="image" style="display:none;" />
                    </div>
                       
                    <div>
                        <label for="vehicle1" class="pb-3 ">
                            <input type="checkbox" name="agree" value="1">
                             I certify that the foregoing information is correct and complete to the best of my knowledge and belief.
                             <p class=" text-danger">{{$errors->first('agree')}}</p>
                        </label><br>
                    </div>
  
                    <div class="col-md-6">
                        <label class="ohnohoney" for="email1"></label>
                        <input class="ohnohoney" autocomplete="off" type="email" id="email1" name="email1" placeholder="Your e-mail here">
                    </div>

                    <div class="form-group">
    		            <input type="hidden" name="jobapply" value="{{ $applyid}}">
	                </div>

                    <button class="btn btn-primary" type="submit">Submit form</button>

                </form>

            </div>

            <hr>



            <div>
                    <p class="py-3 text-xs font-weight-light text-center">Crafted with care by <a href="#" class="text-red">Mizoram State e-Governance Society (MSeGS)</a>, hosted by department of ICT, Government of Mizoram</p>
            </div>



        </div>
    </div>
 

</div>
@endsection