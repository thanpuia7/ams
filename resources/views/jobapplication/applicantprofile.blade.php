
@extends('adminlte::page')
@section('title','Applicant Profile')

@section('content')
<div >
    <div  id='DivIdToPrint' class="position-relative" style =" ">
    
                <div  style ="background-color: rgba(255, 255, 255);" class=" col-xs-12 col-md-12 container rounded border shadow my-md-4 ">


                        <div  class="needs-validation mx-sm-4 mt-3 mt-sm-5 mb-5" >
                        

                            <center>                               
                                <img src="{{ asset('storage/backend/passport/'.$applicant->passportURL) }}" name="aboutme" width="140" height="140" border="0" class="img-circle"></a>
                                <h3 class="media-heading">{{$applicant->name}} <small></small></h3>
                                <p><label>Registration No : {{$applicant->registrationNo}} </label></p>
                                <span><strong>Skills: </strong></span>
                                @if($technicalknowledges != null)
                                    @foreach($technicalknowledges as $technicalknowledge)
                                    <span class="label label-warning">{{$technicalknowledge}}, </span>
                                    @endforeach
                                @else
                                    No Skills
                                @endif
                            </center>
                            <hr>
                            @if($applicant->jobapplys == 1)
                                <center><label>Post Applied :  Project Manager </center>
                            @endif
                            @if($applicant->jobapplys == 2)
                                <center><label>Post Applied :  Senior Management Consultant </center>
                            @endif
                            @if($applicant->jobapplys == 3)
                                <center><label>Post Applied :  Database Administrator </center>
                            @endif
                            @if($applicant->jobapplys == 4)
                                <center><label>Post Applied :  System Administrator </center>
                            @endif
                            @if($applicant->jobapplys == 5)
                                <center><label>Post Applied :  Network & Security Administrator </center>
                            @endif
                            @if($applicant->jobapplys == 6)
                                <center><label>Post Applied :  UI/UX Designer </center>
                            @endif
                            @if($applicant->jobapplys == 7)
                                <center><label>Post Applied :  Software Developer (Frontend) </center>
                            @endif
                            @if($applicant->jobapplys == 8)
                                <center><label>Post Applied :  Software Developer (Backend) </center>
                            @endif
                            @if($applicant->jobapplys == 9)
                                <center><label>Post Applied :  Mentor (Management)</center>
                            @endif
                            @if($applicant->jobapplys == 10)
                                <center><label>Post Applied :  Mentor (Technical) </center>
                            @endif
                            @if($applicant->jobapplys == 11)
                                <center><label>Post Applied :  Co-Ordinator </center>
                            @endif
                          
                            <div class="container">
                                <div class="row">
                                    <div class="col justify-content-left">
                                        <label  for="name ">1. Email: </label> {{$applicant->email}}
                                    </div>
                                    <div class="col justify-content-right">
                                        <label for="passport">2. Gender: </label> {{$applicant->gender}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col justify-content-left">
                                        <label  for="name">3. Mobile Number(For SMS): </label> {{$applicant->phone}}
                                    </div>
                                    <div class="col justify-content-right">
                                        <label for="passport">4. WhatsApp Number: </label> {{$applicant->whatsapp}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col left-align-text">
                                        <label  for="name">5. Religion : </label> {{$applicant->religion}}
                                    </div>
                                    <div class="col right-align-text">
                                        <label for="passport">6. Date of Birth: </label> {{$applicant->dob}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col left-align-text">
                                        <label  for="name">6. Father's/Mother's Name : </label> {{$applicant->fathersname}}
                                    </div>
                                    <div class="col right-align-text">
                                        <label for="passport">7. Caste: </label> {{$applicant->caste}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col left-align-text">
                                        <label  for="name">8. State : </label> {{$applicant->state}}
                                    </div>
                                    <div class="col right-align-text">
                                        <label for="passport">9. District: </label> {{$applicant->district}}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col left-align-text">
                                        <label  for="name">10. Correspondence Address : </label> {{$applicant->caddress}}
                                    </div>

                                </div>

                                <label>11. Educational Qualification</label>
                                <br>
                                <div class ="border px-2 py-2">
                                    <p>I) Class X</p> 
                                    <div class="form-row">                   
                                        <div class="col-md-6">
                                            <p for="passport">a) School:  {{$applicant->university1}}</p>
                                        </div>
                                        <div class="col-md-6">
                                            <p for="passport">b) Year of Passing:  {{$applicant->year1}}</p>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class = "border mt-2 px-2 py-2">
                                    <p  for="name">II) Class XII/Diploma</p> 
                                    <div class="form-row">                               
                                        <div class="col-md-6">
                                            <p for="passport">a) Subject/Branch:  {{$applicant->subject2}}</p>
                                        </div>
                                        <div class="col-md-6">
                                            <p for="passport">b) School/College:  {{$applicant->university2}}</p>
                                        </div>
                                        <div class="col-md-6">
                                            <p for="passport">c) Year of Passing:  {{$applicant->year2}}</p>
                                        </div>
                                    </div>
                                </div>

                                <div class = "border mt-2 px-2 py-2">
                                    <p  for="name">III) Bachelor Degree</p> 
                                    <div class="form-row">                               
                                        <div class="col-md-6">
                                            <p for="passport">a) Subject/Branch:  {{$applicant->subject3}}</p>
                                        </div>
                                        <div class="col-md-6">
                                            <p for="passport">b) University/College:  {{$applicant->university3}}</p>
                                        </div>
                                        <div class="col-md-6">
                                            <p for="passport">c) Year of Passing:  {{$applicant->year3}}</p>
                                        </div>
                                    </div>
                                </div>

                                <div class = "border mt-2 px-2 py-2">
                                    <p  for="name">IV) Master Degree</p> 
                                    <div class="form-row">                               
                                        <div class="col-md-6">
                                        @if($applicant->subject4 != null)
                                            <p for="passport">a) Subject/Branch:  {{$applicant->subject4}}</p>
                                        @else
                                            <p>a) Subject/Branch: ---</p>
                                        @endif
                                        </div>

                                        <div class="col-md-6">
                                        @if($applicant->university4 != null)
                                            <p for="passport">b) University/College:  {{$applicant->university4}}</p>
                                        @else
                                            <p>b) University/College: ---</p>
                                        @endif
                                        </div>

                                        <div class="col-md-6">
                                        @if($applicant->year4 != null)
                                            <p for="passport">c) Year of Passing:  {{$applicant->year4}}</p>
                                        @else
                                            <p>c) Year of Passing: ---</p>
                                        @endif
                                        </div>
                                    </div>
                                </div>


                                <br>
                                <label>12. Professional Course Attended</label>      
                                @if($applicant->coursename1 != null || $applicant->period1 != null || $applicant->institute1 != null)                       
                                <div class = "border mt-2 px-2 py-2">                              
                                    <p  for="name">1. </p> 
                                    <div class="form-row">                               
                                        <div class="col-md-6">
                                        @if($applicant->coursename1 != null)
                                            <p for="passport">a) Course Name:  {{$applicant->coursename1}}</p>
                                        @else
                                            <p>a) Course Name: ---</p>
                                        @endif
                                        </div>

                                        <div class="col-md-6">
                                        @if($applicant->period1 != null)
                                            <p for="passport">b) Period:  {{$applicant->period1}}</p>
                                        @else
                                            <p>b) Period: ---</p>
                                        @endif
                                        </div>

                                        <div class="col-md-6">
                                        @if($applicant->institute1 != null)
                                            <p for="passport">c) Institute:  {{$applicant->institute1}}</p>
                                        @else
                                            <p>c) Institute: ---</p>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @if($applicant->coursename2 != null || $applicant->period2 != null || $applicant->institute2 != null) 
                                <div class = "border mt-2 px-2 py-2">
                                    <p  for="name">2. </p> 
                                    <div class="form-row">                               
                                        <div class="col-md-6">
                                        @if($applicant->coursename2 != null)
                                            <p for="passport">a) Course Name:  {{$applicant->coursename2}}</p>
                                        @else
                                            <p>a) Course Name: ---</p>
                                        @endif
                                        </div>

                                        <div class="col-md-6">
                                        @if($applicant->period2 != null)
                                            <p for="passport">b) Period:  {{$applicant->period2}}</p>
                                        @else
                                            <p>b) Period: ---</p>
                                        @endif
                                        </div>

                                        <div class="col-md-6">
                                        @if($applicant->institute2 != null)
                                            <p for="passport">c) Institute:  {{$applicant->institute2}}</p>
                                        @else
                                            <p>c) Institute: ---</p>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                @endif
                                
                                @if($applicant->coursename3 != null || $applicant->period3 != null || $applicant->institute3 != null) 
                                <div class = "border mt-2 px-2 py-2">
                                    <p  for="name">3. </p> 
                                    <div class="form-row">                               
                                        <div class="col-md-6">
                                        @if($applicant->coursename3 != null)
                                            <p for="passport">a) Course Name:  {{$applicant->coursename3}}</p>
                                        @else
                                            <p>a) Course Name: ---</p>
                                        @endif
                                        </div>

                                        <div class="col-md-6">
                                        @if($applicant->period3 != null)
                                            <p for="passport">b) Period:  {{$applicant->period3}}</p>
                                        @else
                                            <p>b) Period: ---</p>
                                        @endif
                                        </div>

                                        <div class="col-md-6">
                                        @if($applicant->institute3 != null)
                                            <p for="passport">c) Institute:  {{$applicant->institute3}}</p>
                                        @else
                                            <p>c) Institute: ---</p>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                @endif



                                <br>
                                <label>13. Work Experience</label>      
                                @if($applicant->postname1 != null || $applicant->organizationname1 != null || $applicant->duration1 != null)                       
                                <div class = "border mt-2 px-2 py-2">                              
                                    <p  for="name">1. </p> 
                                    <div class="form-row">                               
                                        <div class="col-md-6">
                                        @if($applicant->postname1 != null)
                                            <p for="passport">a) Post Name:  {{$applicant->postname1}}</p>
                                        @else
                                            <p>a) Post Name: ---</p>
                                        @endif
                                        </div>

                                        <div class="col-md-6">
                                        @if($applicant->organizationname1 != null)
                                            <p for="passport">b) Organization Name:  {{$applicant->organizationname1}}</p>
                                        @else
                                            <p>b) Organization Name: ---</p>
                                        @endif
                                        </div>

                                        <div class="col-md-6">
                                        @if($applicant->duration1 != null)
                                            <p for="passport">c) Duration:  {{$applicant->duration1}}</p>
                                        @else
                                            <p>c) Duration: ---</p>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @if($applicant->postname2 != null || $applicant->organizationname2 != null || $applicant->duration2 != null)                       
                                <div class = "border mt-2 px-2 py-2">                              
                                    <p  for="name">2. </p> 
                                    <div class="form-row">                               
                                        <div class="col-md-6">
                                        @if($applicant->postname2 != null)
                                            <p for="passport">a) Post Name:  {{$applicant->postname2}}</p>
                                        @else
                                            <p>a) Post Name: ---</p>
                                        @endif
                                        </div>

                                        <div class="col-md-6">
                                        @if($applicant->organizationname2 != null)
                                            <p for="passport">b) Organization Name:  {{$applicant->organizationname2}}</p>
                                        @else
                                            <p>b) Organization Name: ---</p>
                                        @endif
                                        </div>

                                        <div class="col-md-6">
                                        @if($applicant->duration2 != null)
                                            <p for="passport">c) Duration:  {{$applicant->duration2}}</p>
                                        @else
                                            <p>c) Duration: ---</p>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                @endif
                                
                                @if($applicant->postname3 != null || $applicant->organizationname3 != null || $applicant->duration3 != null)                       
                                <div class = "border mt-2 px-2 py-2">                              
                                    <p  for="name">3. </p> 
                                    <div class="form-row">                               
                                        <div class="col-md-6">
                                        @if($applicant->postname3 != null)
                                            <p for="passport">a) Post Name:  {{$applicant->postname3}}</p>
                                        @else
                                            <p>a) Post Name: ---</p>
                                        @endif
                                        </div>

                                        <div class="col-md-6">
                                        @if($applicant->organizationname3 != null)
                                            <p for="passport">b) Organization Name:  {{$applicant->organizationname3}}</p>
                                        @else
                                            <p>b) Organization Name: ---</p>
                                        @endif
                                        </div>

                                        <div class="col-md-6">
                                        @if($applicant->duration3 != null)
                                            <p for="passport">c) Duration:  {{$applicant->duration3}}</p>
                                        @else
                                            <p>c) Duration: ---</p>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                @endif

                                @if($applicant->postname4 != null || $applicant->organizationname4 != null || $applicant->duration4 != null)                       
                                <div class = "border mt-2 px-2 py-2">                              
                                    <p  for="name">4. </p> 
                                    <div class="form-row">                               
                                        <div class="col-md-6">
                                        @if($applicant->postname4 != null)
                                            <p for="passport">a) Post Name:  {{$applicant->postname4}}</p>
                                        @else
                                            <p>a) Post Name: ---</p>
                                        @endif
                                        </div>

                                        <div class="col-md-6">
                                        @if($applicant->organizationname4 != null)
                                            <p for="passport">b) Organization Name:  {{$applicant->organizationname4}}</p>
                                        @else
                                            <p>b) Organization Name: ---</p>
                                        @endif
                                        </div>

                                        <div class="col-md-6">
                                        @if($applicant->duration4 != null)
                                            <p for="passport">c) Duration:  {{$applicant->duration4}}</p>
                                        @else
                                            <p>c) Duration: ---</p>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                @endif

                                @if($applicant->postname5 != null || $applicant->organizationname5 != null || $applicant->duration5 != null)                       
                                <div class = "border mt-2 px-2 py-2">                              
                                    <p  for="name">5. </p> 
                                    <div class="form-row">                               
                                        <div class="col-md-6">
                                        @if($applicant->postname5 != null)
                                            <p for="passport">a) Post Name:  {{$applicant->postname5}}</p>
                                        @else
                                            <p>a) Post Name: ---</p>
                                        @endif
                                        </div>

                                        <div class="col-md-6">
                                        @if($applicant->organizationname5 != null)
                                            <p for="passport">b) Organization Name:  {{$applicant->organizationname5}}</p>
                                        @else
                                            <p>b) Organization Name: ---</p>
                                        @endif
                                        </div>

                                        <div class="col-md-6">
                                        @if($applicant->duration5 != null)
                                            <p for="passport">c) Duration:  {{$applicant->duration5}}</p>
                                        @else
                                            <p>c) Duration: ---</p>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                @endif

                                <br>
                                <br>

                                <a href="{{route('applicantCertificate',$applicant->id)}}"><button class="btn btn-info">View Certificate</button></a>
                                @if($applicant->jobapplys != 11)
                                <a href="{{route('downloadPortfolio',$applicant->id)}}" target="_blank"><button class="btn btn-info">View Portfolio</button></a>
                                @endif
                            </div>

                        </div>
                        <hr>

                        <div class="modal-footer">
                            <center>
                                <input type='button' id='btn' value='Print' onclick='printDiv();'>
                            </center>
                        </div>

                </div>
    </div>
</div>
<!-- <div>
            <p class="py-3 text-xs font-weight-light text-center">Crafted with care by <a href="#" class="text-red">Mizoram State e-Governance Society (MSeGS)</a>, hosted by department of ICT, Government of Mizoram</p>
</div> -->


@stop

 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> 


 <script type='text/javascript'>

function printDiv() 
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
</script>

<script src="/js/print.js"></script>

</body>
</html>

