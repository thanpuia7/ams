

@extends('adminlte::page')
@section('title','Applicant List')

@section('content')
	
<div class="card">
    @if (\Session::has('message'))
    <center><p class="mx-2 mt-2 text-danger">{!! \Session::get('message') !!}</p></center>
    @endif
  <div class="card-header">
    <h3 class="card-title">List of Applicant </h3>   
  </div>
              <!-- /.card-header -->
  <div class="table-responsive card-body w-full col-md-12 text-nowrap">
    <table id="example2" class="table table-striped table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Applicant Name</th>
          <th>Email</th>
          <th>Phone Number</th>
          <th>Gender</th>
          <th>Registration No</th>
          <th>Action</th>
          <th >View</th>
        </tr>
      </thead>
      
      <tbody>  
        @foreach($Backenddeveloper as $applicant)
        <tr>
          <td>{{$applicant->id}}</td>
          <td>{{$applicant->name}}</td>
          <td>{{$applicant->email}}</td>
          <td>{{$applicant->phone}}</td>
          <td>{{$applicant->gender}}</td>
          <td>{{$applicant->registrationNo}}</td>
          <td><a href="{{route('deletePage',$applicant->id)}}"><button class="btn btn-danger">Delete</button></a></td>         
          <td><a href="{{ route('applicantProfile',$applicant->id) }}" ><button class="btn btn-info" aria-hidden="true" data-content="View">View</button></a></td>
        </tr>

       @endforeach              
      </tbody>
                  <tfoot>
                  <tr>
                    <th>Total: {{$total}}</th>
                    <th>Accepted: {{$accept}}</th>
                    <th>Rejected: {{$reject}}</th>
                    <th>Pending: {{$pending}}</th>

                  </tr>
                  </tfoot>
    </table>
  </div>
              <!-- /.card-body -->
  <div class="paginator container">
        {{ $Backenddeveloper->appends($_GET)->onEachSide(1)->links() }}
  </div>

</div>

  @stop
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
	<script>
		$('.js-pscroll').each(function(){
			var ps = new PerfectScrollbar(this);

			$(window).on('resize', function(){
				ps.update();
			})

			$(this).on('ps-x-reach-start', function(){
				$(this).parent().find('.table100-firstcol').removeClass('shadow-table100-firstcol');
			});

			$(this).on('ps-scroll-x', function(){
				$(this).parent().find('.table100-firstcol').addClass('shadow-table100-firstcol');
			});

		});

		
		
		
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>