@extends('layouts/app')
@section('title','Delete')

@section('content')
<div style ="background-color: rgba(241, 241, 241);" class=" position-relative">
  <div class="position-relative" >
    <div class="position-absolute" style="left:0;right:0;top:20px">
    <div  style ="background-color: rgba(255, 255, 255);" class=" col-xs-12 col-md-6 container rounded border shadow " >
      <center>
        <div class="my-4 mx-2">
          


          <p>Applicant's Registration No. : {{$applicant->registrationNo}} </p>

          <p> Applicant's Name: {{$applicant->name}} </p>

          @if($applicant->jobapplys == 1)
                                  <center><label>Post Applied :  Project Manager </center>
                              @endif
                              @if($applicant->jobapplys == 2)
                                  <center><label>Post Applied :  Senior Management Consultant </center>
                              @endif
                              @if($applicant->jobapplys == 3)
                                  <center><label>Post Applied :  Database Administrator </center>
                              @endif
                              @if($applicant->jobapplys == 4)
                                  <center><label>Post Applied :  System Administrator </center>
                              @endif
                              @if($applicant->jobapplys == 5)
                                  <center><label>Post Applied :  Network & Security Administrator </center>
                              @endif
                              @if($applicant->jobapplys == 6)
                                  <center><label>Post Applied :  UI/UX Designer </center>
                              @endif
                              @if($applicant->jobapplys == 7)
                                  <center><label>Post Applied :  Software Developer (Frontend) </center>
                              @endif
                              @if($applicant->jobapplys == 8)
                                  <center><label>Post Applied :  Software Developer (Backend) </center>
                              @endif
                              @if($applicant->jobapplys == 9)
                                  <center><label>Post Applied :  Mentor (Management </center>
                              @endif
                              @if($applicant->jobapplys == 10)
                                  <center><label>Post Applied :  Mentor (Technical) </center>
                              @endif
                              @if($applicant->jobapplys == 11)
                                  <center><label>Post Applied :  Co-Ordinator </center>
                              @endif

                <form  class="needs-validation mx-sm-4 mt-3 mt-sm-5 mb-5" method="POST" action="{{ route('delete') }}" >
                @csrf

                    <center>
                            <div class="col-md-6 mb-2 pr-2">
                                <label  for="code">Enter the Admin Code</label>
                                <input type="text" name="code" class="form-control" id="code" placeholder="" >
                     
                            </div>

                    </center>

                    <div class="form-group">
    		            <input type="hidden" name="id" value="{{ $id}}">
	                </div>

                    <button class="btn btn-primary" type="submit">Delete</button>
                

                </form>

        </div>
      </center>
    </div>
  </div>

</div>
@endsection