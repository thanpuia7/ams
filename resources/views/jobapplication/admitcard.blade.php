@extends('layouts/app')
@section('title','Applicant Profile')

@section('content')
<div >
    <div  id='DivIdToPrint' class="position-relative mx-4 " style =" ">
    
                <div  style ="background-color: rgba(255, 255, 255);" class=" col-xs-12 col-md-12 container rounded border  shadow my-md-4 ">


                        <div  class="needs-validation mx-sm-4 mt-3 mt-sm-5 mb-5" >

                            <div>
                                <center>
                                    <div class="col">
                                        <a href="https://msegs.mizoram.gov.in"> <img class="img-fluid" src="/image/msegs.png" alt="" style="width:10%; height: 15%; padding:0px"></a>
                                        <h4>Mizoram State e-Governance Society</h4>
                                        <h6 class="text-muted">(A Government of Mizoram Undertaking)</h6>
                                    </div>
                                </center>
                            </div>

                            <hr>
                        

                            <center>   
                                <h3 class="mt-2">Admit Card</h3>                            
                                <img src="{{ asset('storage/backend/passport/'.$applicant->passportURL) }}" name="aboutme" width="140" height="140" border="0" class="img-circle"></a>
                                <h3 class="media-heading">Name: {{$applicant->name}} <small></small></h3>
                                <p><label>Registration No : {{$applicant->registrationNo}} </label></p>
                                
                            </center>
                            <hr>
                            @if($applicant->jobapplys == 1)
                                <center><label>Post Applied :  Project Manager </center>
                            @endif
                            @if($applicant->jobapplys == 2)
                                <center><label>Post Applied :  Senior Management Consultant </center>
                            @endif
                            @if($applicant->jobapplys == 3)
                                <center><label>Post Applied :  Database Administrator </center>
                            @endif
                            @if($applicant->jobapplys == 4)
                                <center><label>Post Applied :  System Administrator </center>
                            @endif
                            @if($applicant->jobapplys == 5)
                                <center><label>Post Applied :  Network & Security Administrator </center>
                            @endif
                            @if($applicant->jobapplys == 6)
                                <center><label>Post Applied :  UI/UX Designer </center>
                            @endif
                            @if($applicant->jobapplys == 7)
                                <center><label>Post Applied :  Software Developer (Frontend) </center>
                            @endif
                            @if($applicant->jobapplys == 8)
                                <center><label>Post Applied :  Software Developer (Backend) </center>
                            @endif
                            @if($applicant->jobapplys == 9)
                                <center><label>Post Applied :  Mentor (Management)</center>
                            @endif
                            @if($applicant->jobapplys == 10)
                                <center><label>Post Applied :  Mentor (Technical) </center>
                            @endif
                            @if($applicant->jobapplys == 11)
                                <center><label>Post Applied :  Co-Ordinator </center>
                            @endif
                          
                            <div class="container">
                                <div class="row">
                                    <div class="col justify-content-left">
                                        <label  for="name ">1. Email: </label> {{$applicant->email}}
                                    </div>
                                    <div class="col justify-content-right">
                                        <label for="passport">2. Gender: </label> {{$applicant->gender}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col justify-content-left">
                                        <label  for="name">3. Mobile Number(For SMS): </label> {{$applicant->phone}}
                                    </div>
                                    <div class="col justify-content-right">
                                        <label for="passport">4. WhatsApp Number: </label> {{$applicant->whatsapp}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col left-align-text">
                                        <label  for="name">5. Religion : </label> {{$applicant->religion}}
                                    </div>
                                    <div class="col right-align-text">
                                        <label for="passport">6. Date of Birth: </label> {{$applicant->dob}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col left-align-text">
                                        <label  for="name">6. Father's/Mother's Name : </label> {{$applicant->fathersname}}
                                    </div>
                                    <div class="col right-align-text">
                                        <label for="passport">7. Caste: </label> {{$applicant->caste}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col left-align-text">
                                        <label  for="name">8. State : </label> {{$applicant->state}}
                                    </div>
                                    <div class="col right-align-text">
                                        <label for="passport">9. District: </label> {{$applicant->district}}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col left-align-text">
                                        <label  for="name">10. Correspondence Address : </label> {{$applicant->caddress}}
                                    </div>

                                </div>
                            
                            </div>

                        </div>
                        <hr>

                        <div class="px-2 py-2 border">
                            <b>Venue:</b> ICT Training Centre, Dr. Kapthuama’s Building, Khatla, Aizawl, Mizoram(Near Assam Rifles Canteen)
                        </div>
                        <div class="row px-2 py-2 ">
                            <div class="col-sm border px-2 py-2">
                                <b>Date of Exam:</b>
                            </div>
                            <div class="col-sm border px-2 py-2">
                                15.12.2020 (Tuesday)
                            </div>
                            <div class="col-sm border px-2 py-2">
                                <b>Time of Exam:</b>
                            </div>
                            @if($applicant->id <=35)
                            <div class="col-sm border px-2 py-2">
                                10:00 Am - 12:00 Pm (First Shift)
                            </div>
                            @endif
                            @if($applicant->id >35 && $applicant->id <= 69)
                            <div class="col-sm border px-2 py-2">
                                12:00 Pm - 02:00 Pm (Second Shift)
                            </div>
                            @endif
                            @if($applicant->id >69)
                            <div class="col-sm border px-2 py-2">
                                02:00 Pm - 04:00 Pm (Third Shift)
                            </div>
                            @endif

                        </div>
                        <hr>
    
<div class="" style="page-break-before:always;">
                        <div class="border px-4 py-4 ">

                        <h3>INSTRUCTIONS TO CANDIDATES</h3>

                            <p>1.	All candidates are advised to strictly follow Standard Operating Procedure (SOP) for prevention of Covid-19 issued by the Govt. of Mizoram from time to time.
                            <p>2.	Candidates will be admitted to the Examination Hall on production of this Admit Card.
                            <p>3.	Admission will ordinarily be refused to a candidate who is more than half an hour late.

                            <p>4.	Candidates will be allotted seats marked with their Roll Nos.  Seats should be occupied before the commencement of the Examination.

                            <p>5.	Candidates should bring their requirements such as Pen/ Pencil etc. for use in the Examination Hall.

                            <p>6.	The candidates should not bring any articles other than those specified above.  However, they will be allowed to use battery operated simple pocket calculator or scientific calculator subject to the nature of question.

                            <p>7.	Candidates must write in his own hand.  Answers must not be written in pencil. Pencils may, however, be used for drawing or rough work.

                            <p>8.	The candidates must attempt questions in accordance with the directions on each question paper. If the questions are attempted in excess of the prescribed number, only the questions attempted first up to the prescribed number shall be valued and the remaining ignored.

                            <p>9.	Candidates detected in using unfair means or communicating with one another or found in possession of unauthorised books and papers during the examination shall be expelled and their names struck off the rolls. 

                            <p>10.	No candidate shall leave the Examination Hall without prior permission of the Invigilator.

                            <p>11.	They must not write or revise their answers after the expiry of the allotted time.

                            <p>12.	Silence must be observed in the Examination Hall.

                            <p>13.	Smoking in the Examination Hall is strictly prohibited.

                            <p>14.	Any candidate found to be intoxicated with alcohol and/or drugs will be expelled from the Examination Hall.
                            <p>15.	Mobile phones, if any, brought to the Examination will have to be left outside the Examination Hall and the Society will not be responsible for their safe keeping.


                        </div>
</div>

              

                        <div class="modal-footer">
                            <center>
                                <input type='button' id='btn' value='Print' onclick='printDiv();'>
                            </center>
                        </div>

                </div>
    </div>
</div>
<!-- <div>
            <p class="py-3 text-xs font-weight-light text-center">Crafted with care by <a href="#" class="text-red">Mizoram State e-Governance Society (MSeGS)</a>, hosted by department of ICT, Government of Mizoram</p>
</div> -->


@stop

 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> 


 <script type='text/javascript'>

function printDiv() 
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
</script>

<script src="/js/print.js"></script>

</body>
</html>

