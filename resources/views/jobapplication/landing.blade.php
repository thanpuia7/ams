@extends('layouts/app')
@section('title','Home')

@section('content')
<div style ="background-color: rgba(242, 240, 240);" class="">

      <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle"><center>Please read the instruction carefully</center></h5>

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <center><h6>INSTRUCTION TO APPLICANTS</h6></center>
            <p>1. The application should be submitted on or before 16th November, 2020 at 3:00 PM. The date and time for practical test and personal interview will be informed to the eligible applicants through e-Mail or Whatsapp</p>
            <p>2. All applicants are requested to submit their application through online only. The relevant documents like Marksheet and Certificates for HSLC, HSSLC, Graduate, Post Graduate and Experience Certificate must be submitted alongwith the application.<p>
            <p>3. Practical test and personal interview will be conducted through online for which the applicants are requested to use their own personal computer/Laptop, broadband Internet connection, webcam and microphone.<p>
            <p>4.	All the applicants are requested to download and install CISCO Webex in their system before conducting the test and interview. The candidates will be given user ID and password through their e-Mail and mobile.</p>
            <p>5.	The applicant for the post of technical and management stream must prepare their project portfolio, if any, and submit alongwith the application. The following are the template for preparing portfolio:</p>
              <ul>
                <li>Project Name</li>
                <li>Project Screenshot</li>
                <li>Project Description </li>
                <li>Project duration</li>
                <li>Technology uses</li>
                <li>Role in the project</li>
                <li>Project website link </li>
              </ul>
            <p>6.	For technical posts such as DB Admin, System Admin, Network Admin, Software Developer, and Mentor Technical, the following must be ready in their system:</p>
              <ul>
                  <li>LAMP/WAMP/MAMP package</li>
                  <li>LDAP, DNS, Mail, and Proxy</li>
                  <li>Git Account</li>
                  <li>CMS Package – WordPress</li>
              </ul>
            <p>7.	For UX/UI Designer post, the applicants are requested to get ready the following software in their system:</p>
              <ul>
                  <li>Adobe Photoshop, Adobe Illustrator, Adobe XD/Sketch/Figma, Webflow/WordPress, Invision, etc.</li>
              </ul>
            <p>8.	For Software Developer post, the applicants must prepare, in advance, a simple web application project in any language of their choice based on which the interviewer will ask question. </p>        
            <p>9.	All jpg images and pdf files to be uploaded must be below 2MB in size</p>  
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            
          </div>
        </div>
      </div>
    </div>

    <!-- <div class="d-none d-lg-block "> -->
    <div class=" ">
        <div style="height: auto; background-color: rgba(73, 114, 222);" class=" ">   
            <CENTER> 
                <div class="container ">
                    <h5 class="text-white pt-5">JOB VACANCIES AT </h5>
                    <h5 class="text-white ">MIZORAM STATE E-GOVERNANCE SOCIETY</h5>
                    <h5 class="text-white ">(AN AUTONOMOUS SOCIETY UNDER GOVERNMENT OF MIZORAM)</h5>
                    <button type="button" class="btn btn-info mt-2 mb-2 " data-toggle="modal" data-target="#exampleModalCenter">Instruction</button>
                    <p class="text-white">(Before Applying please read the instruction carefully)</p>
                    <br>
                </div>
            </center>
        </div>
    </div>

<!-- <div style ="background-color: rgba(13, 56, 130);" class=" d-lg-none">

<p class="text-white px-3 py-3">He portal hi Mizoram mi leh sa dik tak, Mizoram pawna Covid-19 hrileng vanga tangkhang ten, Mizoram Sawrkar hnena an in report theihna tura buatsaih a ni e (Mizoram chhunga awm mek te tan a ni lo! )</p>

</div> -->

    

        <div class=" mx-sm-1 mx-md-5 align-items-center">
            <table id="" class="table  table-responsive " style="border-collapse: separate;border-spacing: 0 20px;">
                <thead>
                  <tr style="background-color:rgba(255, 255, 255);hover:background-color:rgba(230, 230, 230);" class="table-bordered rounded-top">
                    <th>S.No</th>
                    <th>Name of Post</th>
                    <th>Post</th>
                    <th >Eligibility</th>
                    <th >Emolument/Annum</th>
                  </tr>
                </thead>
                <tbody>  
                  <tr class="table-bordered" style="background-color:rgba(255, 255, 255);">
                    <td>1</td>
                    <td>Project Manager</td>
                    <td>1</td>
                    <td>
                    <p><b>Essential Qualification:</b></p>
                    <p>B.E.(CS/ECE/EE)/ MCA/ MSc (CS/IT) with 5-10 years experience in project management or management consultant in reputed organization</p>
                    <p><b>Desirable:</b></p>
                    <p>Expert in BS15000 process , Information Security Management, IT Service Management, QMS, & Project Management. PMP Certification is a plus.</p>
                    <p>
                        <form  method="GET" action="{{ route('form',1) }}" class="inline">
                        @csrf
                            <button type="submit" class="btn btn-info">Apply</button>
                        </form>
                    </td>
                    <td >Rs. 6-7 Lakh with EPF @12% and travelling allowance as admissible under the society rules</td>
                  </tr>             

                  <tr class="table-bordered" style="background-color:rgba(255, 255, 255);">
                    <td>2</td>
                    <td>Senior Management Consultant</td>
                    <td>1</td>
                    <td>
                    <p><b>Essential Qualification:</b></p>
                    <p>B.E (CS/ECE/IT)/ M.C.A / M.Sc(CS/IT) with minimum 5-7 years experience in IT consultant, IT advisory, IT services, and IT Auditing in reputed Company.</p>
                    <p><b>Desirable:</b></p>
                    <p>Expert in Business Analysis, Business Process Reengineering, Functional Requirement Specification, Preparation of RFP & DPR, and Third Party Auditing (TPA).</p>
                    <p>
                        <form  method="GET" action="{{ route('form',2) }}" class="inline">
                        @csrf
                            <button type="submit" class="btn btn-info">Apply</button>
                        </form>
                    </td>
                    <td >Rs. 6-7 Lakh with EPF @12% and travelling allowance as admissible under the society rules</td>
                  </tr>

                  <tr class="table-bordered" style="background-color:rgba(255, 255, 255);">
                    <td>3</td>
                    <td>Database Administrator</td>
                    <td>1</td>
                    <td>
                    <p><b>Essential Qualification:</b></p>
                    <p>B.E.(CS/ECE/Electrical)/ MCA/ MSc (CS/IT) with 2-3years experience in Database Design, Distributed Database, DB Configuration, DB tuning, Backup & Recovery, Data protection & security.</p>
                    <p><b>Desirable:</b></p>
                    <p> Expert in Red hat Linux or CentOS, My SQL, No SQL, Oracle DB, PL/SQL Programming, Database Administration, Performance tuning, Database cluster, backup and recovery</p>
                    <p>
                        <form  method="GET" action="{{ route('form',3) }}" class="inline">
                        @csrf
                            <button type="submit" class="btn btn-info">Apply</button>
                        </form>
                    </td>
                    <td >Rs. 5-6 Lakh EPF @12% will be provided and travelling allowance @ max. 10% of emoluments based on actual bills</td>
                  </tr>

                  <tr class="table-bordered" style="background-color:rgba(255, 255, 255);">
                    <td>4</td>
                    <td>System Administrator</td>
                    <td>1</td>
                    <td>
                    <p><b>Essential Qualification:</b></p>
                    <p>BE (CS/IT/ECE)/ MCA/MSc (CS/IT) with 2-3 years experience in Linux System Administrator</p>
                    <p><b>Desirable:</b></p>
                    <p>Expert in Linux Admin, Installation and configuration of Apache web server, DNS Server, Open LDAP, Mail Server, Firewall/Proxy Server, My SQL, No SQL, Configure PHP, Virtualization, and Cloud Hosting.</p>
                    <p>RHCA Certification will be plus point.</p>
                    <p>
                        <form  method="GET" action="{{ route('form',4) }}" class="inline">
                        @csrf
                            <button type="submit" class="btn btn-info">Apply</button>
                        </form>
                    </td>
                    <td >Rs. 5-6 Lakh with EPF @12% and travelling allowance as admissible under the society rules</td>
                  </tr>

                  <tr class="table-bordered" style="background-color:rgba(255, 255, 255);">
                    <td>5</td>
                    <td>Network & Security Administrator</td>
                    <td>1</td>
                    <td>
                    <p><b>Essential Qualification:</b></p>
                    <p>BE (CS/IT/ECE)/ MCA/MSc (CS/IT) with 2-3 years experience in Linux Networking, Administrator and Security</p>
                    <p><b>Desirable:</b></p>
                    <p>Expert in LAN/WAN, Network Security, Internetwork Services, DDNS, NIS, Network File sharing, SAMBA, E-Mail, Apache, System Security,System Authentication Services, Securing services, Securing data & communication.</p>
                    <p>RHCE Certification will be plus point.</p>
                    <p>
                        <form  method="GET" action="{{ route('form',5) }}" class="inline">
                        @csrf
                            <button type="submit" class="btn btn-info">Apply</button>
                        </form>
                    </td>
                    <td >Rs. 5-6 Lakh with EPF @12% and travelling allowance as admissible under the society rules</td>
                  </tr>

                  <tr class="table-bordered" style="background-color:rgba(255, 255, 255);">
                    <td>6</td>
                    <td>UI/UX Designer</td>
                    <td>2</td>
                    <td>
                    <p><b>Essential Qualification:</b></p>
                    <p>BE (CS/IT/ECE)/MCA/MSc (CS/IT)/ BFA/BVA with 2-3 years experience in UX/UI Design</p>
                    <p><b>Desirable:</b></p>
                    <p>Expert in Aesthetic design for web & mobile framework, Adobe XD. Sketch, Figma, Web flow, Invision, Illustrator & Photoshop.</p>
                    <p>
                        <form  method="GET" action="{{ route('form',6) }}" class="inline">
                        @csrf
                            <button type="submit" class="btn btn-info">Apply</button>
                        </form>
                    </td>
                    <td >Rs. 5-6 Lakh with EPF @12% and travelling allowance as admissible under the society rules.</td>
                  </tr>

                  <tr class="table-bordered" style="background-color:rgba(255, 255, 255);">
                    <td>7</td>
                    <td>Software Developer (Frontend)</td>
                    <td>2</td>
                    <td>
                    <p><b>Essential Qualification:</b></p>
                    <p>BE (CS/IT/ECE)/MCA/MSc (CS/IT) with 2-3 years experience in Frontend Software Development using open source.</p>
                    <p><b>Desirable:</b></p>
                    <p>Expert in CSS/HTML, JavaScript, jQuery, Front End Frameworks (react/angular/vue.js), CSS Preprocessors, RESTful Services/APIs, Responsive/Mobile Design, CrossBrowser Development, Content Management Systems, Testing/Debugging, Git/Version Control </p>
                    <p>
                        <form  method="GET" action="{{ route('form',7) }}" class="inline">
                        @csrf
                            <button type="submit" class="btn btn-info">Apply</button>
                        </form>
                    </td>
                    <td >Rs. 5-6 Lakh with EPF @12% and travelling allowance as admissible under the society rules</td>
                  </tr>

                  <tr class="table-bordered" style="background-color:rgba(255, 255, 255);">
                    <td>8</td>
                    <td>Software Developer (Backend)</td>
                    <td>2</td>
                    <td>
                    <p><b>Essential Qualification:</b></p>
                    <p>BE (CS/IT/ECE)/MCA/MSc (CS/IT) with 2-3 years experience in Backend Software Development using open source</p>
                    <p><b>Desirable:</b></p>
                    <p>Expert in Apache Server, MySQL, No SQL, Rest API or Jason API, Node JS, Tomcat or JBOSS, PHP or Java, Laravel or Spring MVC, SMS & Payment Gateway Integration, Single sign on authentication</p>
                    <p>
                        <form  method="GET" action="{{ route('form',8) }}" class="inline">
                        @csrf
                            <button type="submit" class="btn btn-info">Apply</button>
                        </form>
                    </td>
                    <td >Rs. 5-6 Lakh with EPF @12% and travelling allowance as admissible under the society rules</td>
                  </tr>

                  <tr class="table-bordered" style="background-color:rgba(255, 255, 255);">
                    <td>9</td>
                    <td>Mentor (Management)</td>
                    <td>1</td>
                    <td>
                    <p><b>Essential Qualification:</b></p>
                    <p>MBA/M.Com with 2-3 years experience in management consultancy services</p>
                    <p><b>Desirable:</b></p>
                    <p>Expert in e-Governance Management, Consultancy, preparation of DPR/BPR/FRS, Project Management, MS Project,. PMP Certification is a plus.</p>
                    <p>
                        <form  method="GET" action="{{ route('form',9) }}" class="inline">
                        @csrf
                            <button type="submit" class="btn btn-info">Apply</button>
                        </form>
                    </td>
                    <td >Rs. 5-6 Lakh with EPF @12% and travelling allowance as admissible under the society rules</td>
                  </tr>

                  <tr class="table-bordered" style="background-color:rgba(255, 255, 255);">
                    <td>10</td>
                    <td>Mentor (Technical)</td>
                    <td>1</td>
                    <td>
                    <p><b>Essential Qualification:</b></p>
                    <p>BE (CS/IT/ECE)/MCA/MSc (CS/IT) with 2-3 years experience in Software Development using open source</p>
                    <p><b>Desirable:</b></p>
                    <p>Expert in preparation of SRS, Software Design Document, Software project Management, Opensource programming skills in Java script, HTML5, CSS, MVC, PHP, MySQL, No SQL, JBOSS, Laravel or Spring, Cloud Hosting, Git/Version Control etc</p>
                    <p>
                        <form  method="GET" action="{{ route('form',10) }}" class="inline">
                        @csrf
                            <button type="submit" class="btn btn-info">Apply</button>
                        </form>
                    </td>
                    <td >Rs. 5-6 Lakh with EPF @12% and travelling allowance as admissible under the society rules</td>
                  </tr>

                  <tr class="table-bordered" style="background-color:rgba(255, 255, 255);">
                    <td>11</td>
                    <td>Co-Ordinator</td>
                    <td>1</td>
                    <td>
                    <p><b>Essential Qualification:</b></p>
                    <p>Any Graduate with 2-3 years experience in reputed training institute</p>
                    <p><b>Desirable:</b></p>
                    <p>Expert in Administration & establishment of training institute, Managing professional training programme, Coordination of team management.</p>
                    <p>
                        <form  method="GET" action="{{ route('form',11) }}" class="inline">
                        @csrf
                            <button type="submit" class="btn btn-info">Apply</button>
                        </form>
                    </td>
                    <td >Rs. 3-4 Lakh with EPF @12% and travelling allowance as admissible under the society rules</td>
                  </tr>



                </tbody>
                  <tfoot>

                  </tfoot>
            </table>

            <div class=" py-1" style="background-color:rgba(255, 255, 255);">
              <center><p class="text-muted mt-1">For technical issues only please contact our Software Developer at 9158086237</p></center>
            </div>
        </div>

                

            <hr>



            <div>
                    <p class="py-3 text-xs font-weight-light text-center">Crafted with care by <a href="#" class="text-red">Mizoram State e-Governance Society (MSeGS)</a>, hosted by department of ICT, Government of Mizoram</p>
            </div>



 
</div>
@endsection