@extends('layouts/app')
@section('title','Admit Card')

@section('content')
<div style ="background-color: rgba(241, 241, 241);" class=" position-relative">
  <div class="position-relative" >
    <div class="position-absolute" style="left:0;right:0;top:20px">
    <div  style ="background-color: rgba(255, 255, 255);" class=" col-xs-12 col-md-6 container rounded border shadow " >
      
        <div class="my-4 mx-2">

            @if (\Session::has('message'))
                <center><p class="mx-2 mt-2 text-danger">{!! \Session::get('message') !!}</p></center>
            @endif
       
          <form  class="needs-validation mx-sm-4 mt-3 mt-sm-5 mb-5" method="POST" action="{{ route('admitcard') }}" enctype="multipart/form-data">
                @csrf
                <div class="pb-3 ">
                    <label  for="name">Enter your Registration Number:</label>
                    <input type="text" name="registration_no" class="form-control" placeholder="Registration Number">
                </div>
                <div class="col-md-6">
                        <label class="ohnohoney" for="email1"></label>
                        <input class="ohnohoney" autocomplete="off" type="email" id="email1" name="email1" placeholder="Your e-mail here">
                </div>

                <button class="btn btn-primary" type="submit">Submit</button>

            </form>
          
        
        </div>
     
    </div>
  </div>

</div>
@endsection