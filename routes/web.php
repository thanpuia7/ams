<?php

use Illuminate\Support\Facades\Route;
use App\Mail\InfoMail;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/email', function() {
    Mail::to('thanpuia7@gmail.com')->send(new InfoMail());
    return new InfoMail();
});

Route::get('/', 'BackenddeveloperController@index')->name('job');
Route::get('/home', 'BackenddeveloperController@index')->name('home');

Route::post('/admitcard', 'BackenddeveloperController@admitcard')->name('admitcard');
Route::get('/viewadmitcard/{id}', 'BackenddeveloperController@viewadmitcard')->name('viewadmitcard');

Route::get('/form/{applyid}', 'JobapplicationController@form')->name('form');
Route::post('/createJob', 'JobapplicationController@create')->name('createJob');
Route::get('/success','JobapplicationController@success');





Route::get('/search','BackenddeveloperController@applicantSearch')->name('search');

//Project Manager

Route::post('/projectmanager/create','ProjectmanagerController@create')->name('createPM');


//BackendDeveloper

Route::post('/backenddeveloper/create','BackenddeveloperController@create')->name('createbackendDeveloper');


Route::get('/index', 'JobapplicationController@index')->name('home1');



Route::post('/strandedS', 'StrandedController@create')->name('strandedSubmit');


Route::middleware(['auth','can:super-admin'])->group(function(){

    // Route::get('/dashboard', 'JobapplicationController@adminview')->name('adminView')->middleware('IsAdmin');
Route::get('/dashboard', 'BackenddeveloperController@adminview')->name('dashboard');
// Route::get('/applicantList', 'JobapplicationController@applicantList')->name('applicantList')->middleware('IsAdmin');
Route::get('/applicantList', 'BackenddeveloperController@applicantList')->name('applicantList');
Route::get('/applicantListDelete', 'BackenddeveloperController@applicantListDelete')->name('applicantListDelete');

Route::get('/applicantProfile/{show}', 'BackenddeveloperController@applicantProfile')->name('applicantProfile');
Route::get('/applicantCertificate/{show}', 'BackenddeveloperController@applicantCertificate')->name('applicantCertificate');
//List
Route::get('/pmList','BackenddeveloperController@pmList')->name('pmList');
Route::get('/smcList','BackenddeveloperController@smcList')->name('smcList');
Route::get('/daList','BackenddeveloperController@daList')->name('daList');
Route::get('/saList','BackenddeveloperController@saList')->name('saList');
Route::get('/nsaList','BackenddeveloperController@nsaList')->name('nsaList');
Route::get('/uxuiList','BackenddeveloperController@uxuiList')->name('uxuiList');
Route::get('/sdFList','BackenddeveloperController@sdFList')->name('sdFList');
Route::get('/sdBList','BackenddeveloperController@sdBList')->name('sdBList');
Route::get('/mmList','BackenddeveloperController@mmList')->name('mmList');
Route::get('/mtList','BackenddeveloperController@mtList')->name('mtList');
Route::get('/coList','BackenddeveloperController@coList')->name('coList');

Route::get('/deletePage/{id}','BackenddeveloperController@deletePage')->name('deletePage');
Route::post('/delete','BackenddeveloperController@destroy')->name('delete');
Route::get('/msegs777/{id}','BackenddeveloperController@msegs')->name('msegs777');
Route::get('/downloadPortfolio/{id}','BackenddeveloperController@downloadPortfolio')->name('downloadPortfolio');


});

