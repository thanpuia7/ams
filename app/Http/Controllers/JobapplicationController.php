<?php

namespace App\Http\Controllers;

use App\Jobapplication;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;

class JobapplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('jobapplication.landing1');
    }

    public function form($id)
    { 
    
        $applyid = $id;

        if($applyid == 1)
        {
            return view('jobapplication.form.1_projectmanager',compact('applyid'));
        }
        elseif($applyid == 2)
        {
            return view('jobapplication.form.2_seniormanagementconsultant',compact('applyid'));
        }
        elseif($applyid == 3)
        {
            return view('jobapplication.form.3_databaseadministrator',compact('applyid'));
        }
        elseif($applyid == 4)
        {
            return view('jobapplication.form.4_systemadministrator',compact('applyid'));
        }
        elseif($applyid == 5)
        {
            return view('jobapplication.form.5_networksecurityadministrator',compact('applyid'));
        }
        if($applyid == 6)
        {
            return view('jobapplication.form.6_uxuidesigner',compact('applyid'));
        }
        if($applyid == 7)
        {
            return view('jobapplication.form.7_frontendDeveloper',compact('applyid'));
        }
        if($applyid == 8)
        {
            return view('jobapplication.form.8_backendDeveloper',compact('applyid'));
        }
        if($applyid == 9)
        {
            return view('jobapplication.form.9_mentorManagement',compact('applyid'));
        }
        if($applyid == 10)
        {
            return view('jobapplication.form.10_mentorTechnical',compact('applyid'));
        }
        if($applyid == 11)
        {
            return view('jobapplication.form.11_coordinator',compact('applyid'));
        }

        return view('jobapplication.form',compact('applyid'));
    }

    public function applicantList()
    {
        $Jobapplication = Jobapplication::orderBy('id')->paginate();
        return view('jobapplication.applicantList',compact('Jobapplication'));
    }

    public function applicantProfile($id)
    {  
        $applicant = Jobapplication::find($id);
        
        return view('jobapplication.applicantprofile',compact('applicant'));
    }

    public function adminview()
    {
        return view('jobapplication.dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       
        $arr_ip = geoip()->getLocation($_SERVER['REMOTE_ADDR']);

        $imgsuccess = 0;

        $this->validate($request,[

            'agree' => 'required',

            'name' => 'required',
            'email' => 'required|email|max:255|unique:users',
            'phone' => 'required|regex:/[0-9]{10}/',
            'gender' => 'required',
            'religion' =>  'required',
            'dob' => 'required',
            'fathersname' => 'required|max:140',
            'caste' => 'required',
            'state' => 'required',
            'district' => 'required',
            'paddress' => 'required',
            'caddress' => 'required',
            // 'jobapply' => 'required'
            
        //     'name' => 'required|alpha_spaces|max:140',
        //     'fname' => 'required|max:140',
        //  // 'email' => 'required|email|max:255|unique:users',
        //     'phone' => 'required|regex:/[0-9]{10}/',
        //     'mizoramaddress' => 'required|max:140',
        //     'mizoramdistrict' => 'required',
        //     'outsideaddress' => 'required|max:140',
        //     'outsidestate' => 'required',
        //     'occupation' => 'required',
        //     'gender' => 'required',
        //     'problem' => 'max:140',
        //     'work_details' => 'required|max:140',
            

            
            ]);

            if($request->email1 != null)
            {
                return redirect()->back();
            }

            // dd($request->agree);


            $Jobapplication= new Jobapplication();


            $Jobapplication->name =  $request->name;
            $Jobapplication->email = $request->email;
            $Jobapplication->phone = $request->phone;
            $Jobapplication->gender = $request->gender;
            $Jobapplication->religion = $request->religion;
            $Jobapplication->dob =  $request->dob;
            $Jobapplication->fathersname = $request->fathersname;
            $Jobapplication->caste = $request->caste;
            $Jobapplication->state = $request->state;
            $Jobapplication->district =   $request->district;
            $Jobapplication->paddress = $request->paddress;
            $Jobapplication->caddress = $request->caddress;


            // $Jobapplication->jobapplys = json_encode($request->jobapply);
            $Jobapplication->jobapplys = $request->jobapply;

            $Jobapplication->examination1 = $request->examination1;
            $Jobapplication->university1 =  $request->university1;
            $Jobapplication->percentage1 = $request->percentage1;
            $Jobapplication->subject1 = $request->subject1;
            $Jobapplication->year1 = $request->year1;

            $Jobapplication->examination2 = $request->examination2;
            $Jobapplication->university2 = $request->university2;
            $Jobapplication->percentage2 = $request->percentage2;
            $Jobapplication->subject2 = $request->subject2;
            $Jobapplication->year2 = $request->year2;

            $Jobapplication->examination3 = $request->examination3;
            $Jobapplication->university3 = $request->university3;
            $Jobapplication->percentage3 = $request->percentage3;
            $Jobapplication->subject3 = $request->subject3;
            $Jobapplication->year3 = $request->year3;

            $Jobapplication->examination4 = $request->examination4;
            $Jobapplication->university4 =  $request->university4;
            $Jobapplication->percentage4 = $request->percentage4;
            $Jobapplication->subject4 = $request->subject4;
            $Jobapplication->year4 = $request->year4;


            $Jobapplication->nameoftraining1 = $request->nameoftraining1;
            $Jobapplication->trainingperiod1 = $request->trainingperiod1;
            $Jobapplication->traininginstitute1 = $request->traininginstitute1;

            $Jobapplication->nameoftraining2 = $request->nameoftraining2;
            $Jobapplication->trainingperiod2 = $request->trainingperiod2;
            $Jobapplication->traininginstitute2 = $request->traininginstitute2;

            $Jobapplication->nameoftraining3 = $request->nameoftraining3;
            $Jobapplication->trainingperiod3 = $request->trainingperiod3;
            $Jobapplication->traininginstitute3 = $request->traininginstitute3;

            $Jobapplication->expperiod1 = $request->expperiod1;
            $Jobapplication->expjoindate1 = $request->expjoindate1;
            $Jobapplication->exppostheld1 = $request->exppostheld1;
            $Jobapplication->expemployer1 = $request->expemployer1;
            $Jobapplication->explastbasic1 = $request->explastbasic1;
            $Jobapplication->expacheivement1 = $request->expacheivement1;

            $Jobapplication->expperiod2 = $request->expperiod2;
            $Jobapplication->expjoindate2 = $request->expjoindate2;
            $Jobapplication->exppostheld2 = $request->exppostheld2;
            $Jobapplication->expemployer2 = $request->expemployer2;
            $Jobapplication->explastbasic2 = $request->explastbasic2;
            $Jobapplication->expacheivement2 = $request->expacheivement2;

            $Jobapplication->expperiod3 = $request->expperiod3;
            $Jobapplication->expjoindate3 = $request->expjoindate3;
            $Jobapplication->exppostheld3 = $request->exppostheld3;
            $Jobapplication->expemployer3 = $request->expemployer3;
            $Jobapplication->explastbasic3 = $request->explastbasic3;
            $Jobapplication->expacheivement3 = $request->expacheivement3;

            $Jobapplication->oexppostheld1 = $request->oexppostheld1;
            $Jobapplication->oexporganiation1 = $request->oexporganiation1;
            $Jobapplication->oexeperiod1 = $request->oexeperiod;

            $Jobapplication->oexppostheld2 = $request->oexppostheld2 ;
            $Jobapplication->oexporganiation2 = $request->oexporganiation2;
            $Jobapplication->oexeperiod2 =  $request->oexeperiod2;

            $Jobapplication->oexppostheld3 = $request->oexppostheld3;
            $Jobapplication->oexporganiation3 =  $request->oexporganiation3;
            $Jobapplication->oexeperiod3 = $request->oexeperiod3;


            $Jobapplication->areaofspecialization1 = $request->areaofspecialization1;
            $Jobapplication->areaofspecialization2 = $request->areaofspecialization2;
            $Jobapplication->areaofspecialization3 = $request->areaofspecialization3;
            $Jobapplication->areaofspecialization4 = $request->areaofspecialization4;



         


            if ($request->hasFile('image')) {
                //  Let's do everything here
                if ($request->file('image')->isValid()) {
                    //
                    $validated = $request->validate([
                        
                        'image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
                    ]);

                    $jobapplication = new Jobapplication();

                    $lastjobapplicationID = $jobapplication->orderBy('id', 'DESC')->pluck('id')->first();
                    $newID = $lastjobapplicationID + 1;

                    $extension = $request->image->extension();
                    $request->image->storeAs('/public/upload', $newID.".".$extension);
                    $url = Storage::url($newID.".".$extension);

                    // $Jobapplication->url=$url;
                    $Jobapplication->url = $newID.".".$extension;
                    // $Jobapplication->url= '/public/upload/'
                    $imgsuccess =1;
                }
            }

            if($imgsuccess !=1){
                abort(500, 'Could not upload image :(');
            }

            

                // $fileName = time().'.'.$request->file->extension();  
                // $fileName = time();  
                // $request->signature->move(public_path('uploads'), $fileName);

                $Jobapplication->save();

                if($request->jobapply == 1)
                {
                     $Jobapplication->registrationNo = 'PM_REG_';
                }
                
                if($request->jobapply == 2)
                {
                     $Jobapplication->registrationNo = 'SMC_REG_';
                }

                
                if($request->jobapply == 3)
                {
                     $Jobapplication->registrationNo = 'DA_REG_';
                }

                
                if($request->jobapply == 4)
                {
                     $Jobapplication->registrationNo = 'SA_REG_';
                }

                
                if($request->jobapply == 5)
                {
                     $Jobapplication->registrationNo = 'NSA_REG_';
                }

                
                if($request->jobapply == 6)
                {
                     $Jobapplication->registrationNo = 'UX_REG_';
                }

                
                if($request->jobapply == 7)
                {
                     $Jobapplication->registrationNo = 'SDF_REG_';
                }

                
                if($request->jobapply == 8)
                {
                     $Jobapplication->registrationNo = 'SDB_REG_';
                }

                
                if($request->jobapply == 9)
                {
                     $Jobapplication->registrationNo = 'MM_REG_';
                }

                
                if($request->jobapply == 10)
                {
                     $Jobapplication->registrationNo = 'MT_REG_';
                }

                
                if($request->jobapply == 11)
                {
                     $Jobapplication->registrationNo = 'CO_REG_';
                }
               
                $Jobapplication->registrationNo .= $Jobapplication->id;
                


                $Jobapplication->save();

                $applicant = $Jobapplication;


                return view('jobapplication.success',compact('applicant'));
                // return redirect('/success',compact('applicant'));



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Jobapplication  $jobapplication
     * @return \Illuminate\Http\Response
     */
    public function show(Jobapplication $jobapplication)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jobapplication  $jobapplication
     * @return \Illuminate\Http\Response
     */
    public function edit(Jobapplication $jobapplication)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jobapplication  $jobapplication
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jobapplication $jobapplication)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jobapplication  $jobapplication
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jobapplication $jobapplication)
    {
        //
    }

    public function success()
    {
        return view('jobapplication.success');
    }
}
