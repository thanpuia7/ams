<?php

namespace App\Http\Controllers;

use App\Backenddeveloper;
use App\Deletejob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;

class BackenddeveloperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('jobapplication.admitcardForm');
    }

    public function admitcard(Request $request)
    {
        $registration_no = $request->registration_no;

        $applicant = DB::table('backenddevelopers')->where('registrationNo',$registration_no)->first();

        if($applicant){
        // $technicalknowledges = json_decode($applicant->technicalknowledges);
        return redirect()->route('viewadmitcard', ['id' => $applicant->id]);
        }
        else return redirect()->route('home')->with('message', 'No data found!');
    }

    public function viewadmitcard($id)
    {
        if(is_numeric($id) == false)
        {
            return redirect()->route('home')->with('message', 'Wrong data entered');
        }

        $applicant = Backenddeveloper::find($id);

        $technicalknowledges = json_decode($applicant->technicalknowledges);
        
        return view('jobapplication.admitcard',compact('applicant', 'technicalknowledges'));



        
    }




    public function adminview()
    {
        $total = DB::table('backenddevelopers')->count();
        $pending = DB::table('backenddevelopers')->where('status','0')->count();
        $accept = DB::table('backenddevelopers')->where('status','1')->count();
        $reject = DB::table('backenddevelopers')->where('status','2')->count();

        //PM
        $total1 = DB::table('backenddevelopers')->where('jobapplys','1')->count();
        $pending1 = DB::table('backenddevelopers')->where('jobapplys','1')->where('status','0')->count();
        $accept1 = DB::table('backenddevelopers')->where('jobapplys','1')->where('status','1')->count();
        $reject1 = DB::table('backenddevelopers')->where('jobapplys','1')->where('status','2')->count();

        //smc
        $total2 = DB::table('backenddevelopers')->where('jobapplys','2')->count();
        $pending2 = DB::table('backenddevelopers')->where('jobapplys','2')->where('status','0')->count();
        $accept2 = DB::table('backenddevelopers')->where('jobapplys','2')->where('status','1')->count();
        $reject2 = DB::table('backenddevelopers')->where('jobapplys','2')->where('status','2')->count();

        //da
        $total3 = DB::table('backenddevelopers')->where('jobapplys','3')->count();
        $pending3 = DB::table('backenddevelopers')->where('jobapplys','3')->where('status','0')->count();
        $accept3 = DB::table('backenddevelopers')->where('jobapplys','3')->where('status','1')->count();
        $reject3 = DB::table('backenddevelopers')->where('jobapplys','3')->where('status','2')->count();

        //sa
        $total4 = DB::table('backenddevelopers')->where('jobapplys','4')->count();
        $pending4 = DB::table('backenddevelopers')->where('jobapplys','4')->where('status','0')->count();
        $accept4 = DB::table('backenddevelopers')->where('jobapplys','4')->where('status','1')->count();
        $reject4 = DB::table('backenddevelopers')->where('jobapplys','4')->where('status','2')->count();

        //nsa
        $total5 = DB::table('backenddevelopers')->where('jobapplys','5')->count();
        $pending5 = DB::table('backenddevelopers')->where('jobapplys','5')->where('status','0')->count();
        $accept5 = DB::table('backenddevelopers')->where('jobapplys','5')->where('status','1')->count();
        $reject5 = DB::table('backenddevelopers')->where('jobapplys','5')->where('status','2')->count();

        //uxui
        $total6 = DB::table('backenddevelopers')->where('jobapplys','6')->count();
        $pending6 = DB::table('backenddevelopers')->where('jobapplys','6')->where('status','0')->count();
        $accept6 = DB::table('backenddevelopers')->where('jobapplys','6')->where('status','1')->count();
        $reject6 = DB::table('backenddevelopers')->where('jobapplys','6')->where('status','2')->count();
        
        //sdF
        $total7 = DB::table('backenddevelopers')->where('jobapplys','7')->count();
        $pending7 = DB::table('backenddevelopers')->where('jobapplys','7')->where('status','0')->count();
        $accept7 = DB::table('backenddevelopers')->where('jobapplys','7')->where('status','1')->count();
        $reject7 = DB::table('backenddevelopers')->where('jobapplys','7')->where('status','2')->count();

        //sdB
        $total8 = DB::table('backenddevelopers')->where('jobapplys','8')->count();
        $pending8 = DB::table('backenddevelopers')->where('jobapplys','8')->where('status','0')->count();
        $accept8 = DB::table('backenddevelopers')->where('jobapplys','8')->where('status','1')->count();
        $reject8 = DB::table('backenddevelopers')->where('jobapplys','8')->where('status','2')->count();

        //mm
        $total9 = DB::table('backenddevelopers')->where('jobapplys','9')->count();
        $pending9 = DB::table('backenddevelopers')->where('jobapplys','9')->where('status','0')->count();
        $accept9 = DB::table('backenddevelopers')->where('jobapplys','9')->where('status','1')->count();
        $reject9 = DB::table('backenddevelopers')->where('jobapplys','9')->where('status','2')->count();

        //mt
        $total10 = DB::table('backenddevelopers')->where('jobapplys','10')->count();
        $pending10 = DB::table('backenddevelopers')->where('jobapplys','10')->where('status','0')->count();
        $accept10 = DB::table('backenddevelopers')->where('jobapplys','10')->where('status','1')->count();
        $reject10 = DB::table('backenddevelopers')->where('jobapplys','10')->where('status','2')->count();

        //co
        $total11 = DB::table('backenddevelopers')->where('jobapplys','11')->count();
        $pending11 = DB::table('backenddevelopers')->where('jobapplys','11')->where('status','0')->count();
        $accept11 = DB::table('backenddevelopers')->where('jobapplys','11')->where('status','1')->count();
        $reject11 = DB::table('backenddevelopers')->where('jobapplys','11')->where('status','2')->count();

        //Deleted Records
                //co
                $totalDelete = DB::table('deletejobs')->count();
                $pendingDelete = DB::table('deletejobs')->where('status','0')->count();
                $acceptDelete = DB::table('deletejobs')->where('status','1')->count();
                $rejectDelete = DB::table('deletejobs')->where('status','2')->count();
        
        return view('jobapplication.dashboard',compact('total','reject','accept','pending','total1','reject1','accept1','pending1','total2','reject2','accept2','pending2','total3','reject3','accept3','pending3','total4','reject4','accept4','pending4','total5','reject5','accept5','pending5','total6','reject6','accept6','pending6','total7','reject7','accept7','pending7','total8','reject8','accept8','pending8','total9','reject9','accept9','pending9','total10','reject10','accept10','pending10','total11','reject11','accept11','pending11','totalDelete','rejectDelete','acceptDelete','pendingDelete'));
    }

    public function applicantList()
    {
        $Backenddeveloper = Backenddeveloper::orderBy('id')->paginate();
        $total = DB::table('backenddevelopers')->count();
        $pending = DB::table('backenddevelopers')->where('status','0')->count();
        $accept = DB::table('backenddevelopers')->where('status','1')->count();
        $reject = DB::table('backenddevelopers')->where('status','2')->count();


        // $accept = DB::table('accept')->where('status','1')->where('division_id','2')->count();
        return view('jobapplication.applicantList',compact('Backenddeveloper','total','reject','accept','pending'));
    }
    
    public function pmList()
    {
        $Backenddeveloper = DB::table('backenddevelopers')->where('jobapplys','1')->paginate();
        $total = DB::table('backenddevelopers')->where('jobapplys','1')->count();
        $pending = DB::table('backenddevelopers')->where('jobapplys','1')->where('status','0')->count();
        $accept = DB::table('backenddevelopers')->where('jobapplys','1')->where('status','1')->count();
        $reject = DB::table('backenddevelopers')->where('jobapplys','1')->where('status','2')->count();

        return view('jobapplication.form.list.projectmanager',compact('Backenddeveloper','total','reject','accept','pending'));
    }

    public function smcList()
    {
        $Backenddeveloper = DB::table('backenddevelopers')->where('jobapplys','2')->paginate();
        $total = DB::table('backenddevelopers')->where('jobapplys','2')->count();
        $pending = DB::table('backenddevelopers')->where('jobapplys','2')->where('status','0')->count();
        $accept = DB::table('backenddevelopers')->where('jobapplys','2')->where('status','1')->count();
        $reject = DB::table('backenddevelopers')->where('jobapplys','2')->where('status','2')->count();

        return view('jobapplication.form.list.list2',compact('Backenddeveloper','total','reject','accept','pending'));
    }

    public function daList()
    {
        $Backenddeveloper = DB::table('backenddevelopers')->where('jobapplys','3')->paginate();
        $total = DB::table('backenddevelopers')->where('jobapplys','3')->count();
        $pending = DB::table('backenddevelopers')->where('jobapplys','3')->where('status','0')->count();
        $accept = DB::table('backenddevelopers')->where('jobapplys','3')->where('status','1')->count();
        $reject = DB::table('backenddevelopers')->where('jobapplys','3')->where('status','2')->count();

        return view('jobapplication.form.list.list3',compact('Backenddeveloper','total','reject','accept','pending'));
    }

    public function saList()
    {
        $Backenddeveloper = DB::table('backenddevelopers')->where('jobapplys','4')->paginate();
        $total = DB::table('backenddevelopers')->where('jobapplys','4')->count();
        $pending = DB::table('backenddevelopers')->where('jobapplys','4')->where('status','0')->count();
        $accept = DB::table('backenddevelopers')->where('jobapplys','4')->where('status','1')->count();
        $reject = DB::table('backenddevelopers')->where('jobapplys','4')->where('status','2')->count();

        return view('jobapplication.form.list.list4',compact('Backenddeveloper','total','reject','accept','pending'));
    }

    public function nsaList()
    {
        $Backenddeveloper = DB::table('backenddevelopers')->where('jobapplys','5')->paginate();
        $total = DB::table('backenddevelopers')->where('jobapplys','5')->count();
        $pending = DB::table('backenddevelopers')->where('jobapplys','5')->where('status','0')->count();
        $accept = DB::table('backenddevelopers')->where('jobapplys','5')->where('status','1')->count();
        $reject = DB::table('backenddevelopers')->where('jobapplys','5')->where('status','2')->count();

        return view('jobapplication.form.list.list5',compact('Backenddeveloper','total','reject','accept','pending'));
    }

    public function uxuiList()
    {
        $Backenddeveloper = DB::table('backenddevelopers')->where('jobapplys','6')->paginate();
        $total = DB::table('backenddevelopers')->where('jobapplys','6')->count();
        $pending = DB::table('backenddevelopers')->where('jobapplys','6')->where('status','0')->count();
        $accept = DB::table('backenddevelopers')->where('jobapplys','6')->where('status','1')->count();
        $reject = DB::table('backenddevelopers')->where('jobapplys','6')->where('status','2')->count();

        return view('jobapplication.form.list.list6',compact('Backenddeveloper','total','reject','accept','pending'));
    }

    public function sdFList()
    {
        $Backenddeveloper = DB::table('backenddevelopers')->where('jobapplys','7')->paginate();
        $total = DB::table('backenddevelopers')->where('jobapplys','7')->count();
        $pending = DB::table('backenddevelopers')->where('jobapplys','7')->where('status','0')->count();
        $accept = DB::table('backenddevelopers')->where('jobapplys','7')->where('status','1')->count();
        $reject = DB::table('backenddevelopers')->where('jobapplys','7')->where('status','2')->count();

        return view('jobapplication.form.list.list7',compact('Backenddeveloper','total','reject','accept','pending'));
    }

    public function sdBList()
    {
        $Backenddeveloper = DB::table('backenddevelopers')->where('jobapplys','8')->paginate();
        $total = DB::table('backenddevelopers')->where('jobapplys','8')->count();
        $pending = DB::table('backenddevelopers')->where('jobapplys','8')->where('status','0')->count();
        $accept = DB::table('backenddevelopers')->where('jobapplys','8')->where('status','1')->count();
        $reject = DB::table('backenddevelopers')->where('jobapplys','8')->where('status','2')->count();

        return view('jobapplication.form.list.list8',compact('Backenddeveloper','total','reject','accept','pending'));
    }

    public function mmList()
    {
        $Backenddeveloper = DB::table('backenddevelopers')->where('jobapplys','9')->paginate();
        $total = DB::table('backenddevelopers')->where('jobapplys','9')->count();
        $pending = DB::table('backenddevelopers')->where('jobapplys','9')->where('status','0')->count();
        $accept = DB::table('backenddevelopers')->where('jobapplys','9')->where('status','1')->count();
        $reject = DB::table('backenddevelopers')->where('jobapplys','9')->where('status','2')->count();

        return view('jobapplication.form.list.list9',compact('Backenddeveloper','total','reject','accept','pending'));
    }

    public function mtList()
    {
        $Backenddeveloper = DB::table('backenddevelopers')->where('jobapplys','10')->paginate();
        $total = DB::table('backenddevelopers')->where('jobapplys','10')->count();
        $pending = DB::table('backenddevelopers')->where('jobapplys','10')->where('status','0')->count();
        $accept = DB::table('backenddevelopers')->where('jobapplys','10')->where('status','1')->count();
        $reject = DB::table('backenddevelopers')->where('jobapplys','10')->where('status','2')->count();

        return view('jobapplication.form.list.list10',compact('Backenddeveloper','total','reject','accept','pending'));
    }

    public function coList()
    {
        $Backenddeveloper = DB::table('backenddevelopers')->where('jobapplys','11')->paginate();
        $total = DB::table('backenddevelopers')->where('jobapplys','11')->count();
        $pending = DB::table('backenddevelopers')->where('jobapplys','11')->where('status','0')->count();
        $accept = DB::table('backenddevelopers')->where('jobapplys','11')->where('status','1')->count();
        $reject = DB::table('backenddevelopers')->where('jobapplys','11')->where('status','2')->count();

        return view('jobapplication.form.list.list11',compact('Backenddeveloper','total','reject','accept','pending'));
    }

    public function applicantListDelete()
    {
        $Deletejob = Deletejob::orderBy('id')->paginate();
        $total = DB::table('deletejobs')->count();
        $pending = DB::table('deletejobs')->where('status','0')->count();
        $accept = DB::table('deletejobs')->where('status','1')->count();
        $reject = DB::table('deletejobs')->where('status','2')->count();


        // $accept = DB::table('accept')->where('status','1')->where('division_id','2')->count();
        return view('jobapplication.applicantListDelete',compact('Deletejob','total','reject','accept','pending'));
    }


    

    
    public function applicantProfile($id)
    {  
        $applicant = Backenddeveloper::find($id);
      
        $technicalknowledges = json_decode($applicant->technicalknowledges);
        return view('jobapplication.applicantprofile',compact('applicant', 'technicalknowledges'));
    }


    
    public function applicantSearch($registrationNo)
    {  
        

        $applicant = DB::table('backenddevelopers')->where('registrationNo',$request->registrationNo)->get();
        
        return view('jobapplication.applicantprofile',compact('applicant'));
    }

    public function applicantCertificate($id)
    {  
        $applicant = Backenddeveloper::find($id);

        $technicalknowledges = json_decode($applicant->technicalknowledges);
        return view('jobapplication.applicantCertificate',compact('applicant', 'technicalknowledges'));
    }

    public function downloadPortfolio($id)
    {
        $applicant = Backenddeveloper::find($id);

        return redirect('/storage/backend/portfolio/'.$applicant->portfolioURL);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

 
       
       

        $imgsuccess = 0;

        $this->validate($request,[

            'agree' => 'required',
            'passport' =>'required|mimes:jpeg,png,jpg,gif,svg|max:4000',                        //1
            'name' => 'required',
            'email' => 'required|email|max:255',
            'gender' => 'required',
            'phone' => 'required|regex:/[0-9]{10}/',
            'whatsapp' => 'required|regex:/[0-9]{10}/',
            'religion' =>  'required',
            'dob' => 'required',
            'fathersname' => 'required|max:140',
            'caste' => 'required',
            'state' => 'required',
            'district' => 'required',
            'caddress' => 'required',

           
            'marksheet1' => 'required|mimes:jpeg,png,jpg,gif,svg|max:4000',                     //3
            'passingcertificate1' => 'required|mimes:jpeg,png,jpg,gif,svg|max:4000',            //4

            'marksheet2' => 'required|mimes:jpeg,png,jpg,gif,svg|max:4000',                     //5
            'passingcertificate2' => 'required|mimes:jpeg,png,jpg,gif,svg|max:4000',            //6

            'marksheet3' => 'required|mimes:jpeg,png,jpg,gif,svg|max:4000',                     //7
            'passingcertificate3' => 'required|mimes:jpeg,png,jpg,gif,svg|max:4000',            //8

            'marksheet4' => 'max:4000',
            'passingcertificate4' => 'max:4000',

            'experiencecertificate1' => 'max:4000',
            'experiencecertificate2' => 'max:4000',
            'experiencecertificate3' => 'max:4000',
            'experiencecertificate4' => 'max:4000',
            'experiencecertificate5' => 'max:4000',
            
            'portfolio' => 'max:4000',



            // 'technicalknowledge' => 'required',

            // 'jobapply' => 'required'
            
        //     'name' => 'required|alpha_spaces|max:140',
        //     'fname' => 'required|max:140',
        //  // 'email' => 'required|email|max:255|unique:users',
        //     'phone' => 'required|regex:/[0-9]{10}/',
        //     'mizoramaddress' => 'required|max:140',
        //     'mizoramdistrict' => 'required',
        //     'outsideaddress' => 'required|max:140',
        //     'outsidestate' => 'required',
        //     'occupation' => 'required',
        //     'gender' => 'required',
        //     'problem' => 'max:140',
        //     'work_details' => 'required|max:140',
            

            
            ]);

            if ($request->jobapply != 11) {
                $this->validate($request,[
                'portfolio' => 'required|mimes:jpeg,png,jpg,gif,svg,pdf|max:4000',                      //2
                ]);
            }


            if($request->email1 != null)
            {
                return redirect()->back();
            }

            // dd($request->agree);


            $Backenddeveloper= new Backenddeveloper();


            $Backenddeveloper->name =  $request->name;
            $Backenddeveloper->email = $request->email;
            $Backenddeveloper->gender = $request->gender;
            $Backenddeveloper->phone = $request->phone;
            $Backenddeveloper->whatsapp = $request->whatsapp;
            $Backenddeveloper->religion = $request->religion;
            $Backenddeveloper->dob =  $request->dob;
            $Backenddeveloper->fathersname = $request->fathersname;
            $Backenddeveloper->caste = $request->caste;
            $Backenddeveloper->state = $request->state;
            $Backenddeveloper->district =   $request->district;
            $Backenddeveloper->caddress = $request->caddress;


            // $Jobapplication->jobapplys = json_encode($request->jobapply);
            // $Jobapplication->jobapplys = $request->jobapply;


            $Backenddeveloper->subject1 = $request->subject1;
            $Backenddeveloper->university1 =  $request->university1;
            $Backenddeveloper->year1 = $request->year1;

            $Backenddeveloper->subject2 = $request->subject2;
            $Backenddeveloper->university2 =  $request->university2;
            $Backenddeveloper->year2 = $request->year2;

            $Backenddeveloper->subject3 = $request->subject3;
            $Backenddeveloper->university3 =  $request->university3;
            $Backenddeveloper->year3 = $request->year3;

            $Backenddeveloper->subject4 = $request->subject4;
            $Backenddeveloper->university4 =  $request->university4;
            $Backenddeveloper->year4 = $request->year4;



            $Backenddeveloper->coursename1 = $request->coursename1;
            $Backenddeveloper->period1 =  $request->period1;
            $Backenddeveloper->institute1 = $request->institute1;

            $Backenddeveloper->coursename2 = $request->coursename2;
            $Backenddeveloper->period2 =  $request->period2;
            $Backenddeveloper->institute2 = $request->institute2;

            $Backenddeveloper->coursename3 = $request->coursename3;
            $Backenddeveloper->period3 =  $request->period3;
            $Backenddeveloper->institute3 = $request->institute3;



            $Backenddeveloper->postname1 = $request->postname1;
            $Backenddeveloper->organizationname1 =  $request->organizationname1;
            $Backenddeveloper->duration1 = $request->duration1;

            $Backenddeveloper->postname2 = $request->postname2;
            $Backenddeveloper->organizationname2 =  $request->organizationname2;
            $Backenddeveloper->duration2 = $request->duration2;

            $Backenddeveloper->postname3 = $request->postname3;
            $Backenddeveloper->organizationname3 =  $request->organizationname3;
            $Backenddeveloper->duration3 = $request->duration3;
            
            $Backenddeveloper->postname4 = $request->postname4;
            $Backenddeveloper->organizationname4 =  $request->organizationname4;
            $Backenddeveloper->duration4 = $request->duration4;

            $Backenddeveloper->postname5 = $request->postname5;
            $Backenddeveloper->organizationname5 =  $request->organizationname5;
            $Backenddeveloper->duration5 = $request->duration5;
           


            $Backenddeveloper->technicalknowledges = json_encode($request->technicalknowledge);
            $Backenddeveloper->jobapplys = $request->jobapply;
        
            $Backenddeveloper->status = 0;
           
            

         
            //This is for PASSPORT UPLOAD

            if ($request->hasFile('passport')) {
                //  Let's do everything here
                if ($request->file('passport')->isValid()) {
                    //
                    $validated = $request->validate([
                        
                        'passport' => 'mimes:jpeg,png,jpg,gif,svg|max:4000',
                    ]);

                    $backenddeveloper = new Backenddeveloper();

                    $lastbackenddeveloperID = $backenddeveloper->orderBy('id', 'DESC')->pluck('id')->first();
                    $newID = $lastbackenddeveloperID + 1;

                    $extension = $request->passport->extension();
                    $request->passport->storeAs('/public/backend/passport', $newID.".".$extension);
                    $url = Storage::url($newID.".".$extension);

                    // $Jobapplication->url=$url;
                    $Backenddeveloper->passportURL = $newID.".".$extension;
                    // $Jobapplication->url= '/public/upload/'
                    $imgsuccess =1;
                }
            }

            if($imgsuccess !=1){
                abort(500, 'Could not upload image :(');
            }


            //--> End of PASSPORT UPLOAD


            //-->This is for Educational Qualification

                        if ($request->hasFile('marksheet1')) {
                            //  Let's do everything here
                            if ($request->file('marksheet1')->isValid()) {
                                //
                                $validated = $request->validate([
                                    
                                    'marksheet1' => 'mimes:jpeg,png,jpg,gif,svg|max:4000',
                                ]);
            
                                $backenddeveloper = new Backenddeveloper();
            
                                $lastbackenddeveloperID = $backenddeveloper->orderBy('id', 'DESC')->pluck('id')->first();
                                $newID = $lastbackenddeveloperID + 1;
            
                                $extension = $request->marksheet1->extension();
                                $request->marksheet1->storeAs('/public/backend/marksheet1', $newID.".".$extension);
                                $url = Storage::url($newID.".".$extension);
            
                                // $Jobapplication->url=$url;
                                $Backenddeveloper->marksheet1URL = $newID.".".$extension;
                                // $Jobapplication->url= '/public/upload/'
                                $imgsuccess =1;
                            }
                        }
            
                        if($imgsuccess !=1){
                            abort(500, 'Could not upload image :(');
                        }

                        if ($request->hasFile('passingcertificate1')) {
                            //  Let's do everything here
                            if ($request->file('passingcertificate1')->isValid()) {
                                //
                                $validated = $request->validate([
                                    
                                    'passingcertificate1' => 'mimes:jpeg,png,jpg,gif,svg|max:4000',
                                ]);
            
                                $backenddeveloper = new Backenddeveloper();
            
                                $lastbackenddeveloperID = $backenddeveloper->orderBy('id', 'DESC')->pluck('id')->first();
                                $newID = $lastbackenddeveloperID + 1;
            
                                $extension = $request->passingcertificate1->extension();
                                $request->passingcertificate1->storeAs('/public/backend/passingcertificate1', $newID.".".$extension);
                                $url = Storage::url($newID.".".$extension);
            
                                // $Jobapplication->url=$url;
                                $Backenddeveloper->passingcertificate1URL = $newID.".".$extension;
                                // $Jobapplication->url= '/public/upload/'
                                $imgsuccess =1;
                            }
                        }
            
                        if($imgsuccess !=1){
                            abort(500, 'Could not upload image :(');
                        }



                        if ($request->hasFile('marksheet2')) {
                            //  Let's do everything here
                            if ($request->file('marksheet2')->isValid()) {
                                //
                                $validated = $request->validate([
                                    
                                    'marksheet2' => 'mimes:jpeg,png,jpg,gif,svg|max:4000',
                                ]);
            
                                $backenddeveloper = new Backenddeveloper();
            
                                $lastbackenddeveloperID = $backenddeveloper->orderBy('id', 'DESC')->pluck('id')->first();
                                $newID = $lastbackenddeveloperID + 1;
            
                                $extension = $request->marksheet2->extension();
                                $request->marksheet2->storeAs('/public/backend/marksheet2', $newID.".".$extension);
                                $url = Storage::url($newID.".".$extension);
            
                                // $Jobapplication->url=$url;
                                $Backenddeveloper->marksheet2URL = $newID.".".$extension;
                                // $Jobapplication->url= '/public/upload/'
                                $imgsuccess =1;
                            }
                        }
            
                        if($imgsuccess !=1){
                            abort(500, 'Could not upload image :(');
                        }

                        if ($request->hasFile('passingcertificate2')) {
                            //  Let's do everything here
                            if ($request->file('passingcertificate2')->isValid()) {
                                //
                                $validated = $request->validate([
                                    
                                    'passingcertificate2' => 'mimes:jpeg,png,jpg,gif,svg|max:4000',
                                ]);
            
                                $backenddeveloper = new Backenddeveloper();
            
                                $lastbackenddeveloperID = $backenddeveloper->orderBy('id', 'DESC')->pluck('id')->first();
                                $newID = $lastbackenddeveloperID + 1;
            
                                $extension = $request->passingcertificate2->extension();
                                $request->passingcertificate2->storeAs('/public/backend/passingcertificate2', $newID.".".$extension);
                                $url = Storage::url($newID.".".$extension);
            
                                // $Jobapplication->url=$url;
                                $Backenddeveloper->passingcertificate2URL = $newID.".".$extension;
                                // $Jobapplication->url= '/public/upload/'
                                $imgsuccess =1;
                            }
                        }
            
                        if($imgsuccess !=1){
                            abort(500, 'Could not upload image :(');
                        }

                        if ($request->hasFile('marksheet3')) {
                            //  Let's do everything here
                            if ($request->file('marksheet3')->isValid()) {
                                //
                                $validated = $request->validate([
                                    
                                    'marksheet3' => 'mimes:jpeg,png,jpg,gif,svg|max:4000',
                                ]);
            
                                $backenddeveloper = new Backenddeveloper();
            
                                $lastbackenddeveloperID = $backenddeveloper->orderBy('id', 'DESC')->pluck('id')->first();
                                $newID = $lastbackenddeveloperID + 1;
            
                                $extension = $request->marksheet3->extension();
                                $request->marksheet3->storeAs('/public/backend/marksheet3', $newID.".".$extension);
                                $url = Storage::url($newID.".".$extension);
            
                                // $Jobapplication->url=$url;
                                $Backenddeveloper->marksheet3URL = $newID.".".$extension;
                                // $Jobapplication->url= '/public/upload/'
                                $imgsuccess =1;
                            }
                        }
            
                        if($imgsuccess !=1){
                            abort(500, 'Could not upload image :(');
                        }

                        if ($request->hasFile('passingcertificate3')) {
                            //  Let's do everything here
                            if ($request->file('passingcertificate3')->isValid()) {
                                //
                                $validated = $request->validate([
                                    
                                    'passingcertificate3' => 'mimes:jpeg,png,jpg,gif,svg|max:4000',
                                ]);
            
                                $backenddeveloper = new Backenddeveloper();
            
                                $lastbackenddeveloperID = $backenddeveloper->orderBy('id', 'DESC')->pluck('id')->first();
                                $newID = $lastbackenddeveloperID + 1;
            
                                $extension = $request->passingcertificate3->extension();
                                $request->passingcertificate3->storeAs('/public/backend/passingcertificate3', $newID.".".$extension);
                                $url = Storage::url($newID.".".$extension);
            
                                // $Jobapplication->url=$url;
                                $Backenddeveloper->passingcertificate3URL = $newID.".".$extension;
                                // $Jobapplication->url= '/public/upload/'
                                $imgsuccess =1;
                            }
                        }
            
                        if($imgsuccess !=1){
                            abort(500, 'Could not upload image :(');
                        }


                        if ($request->hasFile('marksheet4')) {
                            //  Let's do everything here
                            if ($request->file('marksheet4')->isValid()) {
                                //
                                $validated = $request->validate([
                                    
                                    'marksheet4' => 'mimes:jpeg,png,jpg,gif,svg|max:4000',
                                ]);
            
                                $backenddeveloper = new Backenddeveloper();
            
                                $lastbackenddeveloperID = $backenddeveloper->orderBy('id', 'DESC')->pluck('id')->first();
                                $newID = $lastbackenddeveloperID + 1;
            
                                $extension = $request->marksheet4->extension();
                                $request->marksheet4->storeAs('/public/backend/marksheet4', $newID.".".$extension);
                                $url = Storage::url($newID.".".$extension);
            
                                // $Jobapplication->url=$url;
                                $Backenddeveloper->marksheet4URL = $newID.".".$extension;
                                // $Jobapplication->url= '/public/upload/'
                                $imgsuccess =1;
                            }
                        }
            
                        if($imgsuccess !=1){
                            abort(500, 'Could not upload image :(');
                        }

                        if ($request->hasFile('passingcertificate4')) {
                            //  Let's do everything here
                            if ($request->file('passingcertificate4')->isValid()) {
                                //
                                $validated = $request->validate([
                                    
                                    'passingcertificate4' => 'mimes:jpeg,png,jpg,gif,svg|max:4000',
                                ]);
            
                                $backenddeveloper = new Backenddeveloper();
            
                                $lastbackenddeveloperID = $backenddeveloper->orderBy('id', 'DESC')->pluck('id')->first();
                                $newID = $lastbackenddeveloperID + 1;
            
                                $extension = $request->passingcertificate4->extension();
                                $request->passingcertificate4->storeAs('/public/backend/passingcertificate4', $newID.".".$extension);
                                $url = Storage::url($newID.".".$extension);
            
                                // $Jobapplication->url=$url;
                                $Backenddeveloper->passingcertificate4URL = $newID.".".$extension;
                                // $Jobapplication->url= '/public/upload/'
                                $imgsuccess =1;
                            }
                        }
            
                        if($imgsuccess !=1){
                            abort(500, 'Could not upload image :(');
                        }

                           
            //--> End of Educational Qualification Upload


            //--> Start Experience Certificate

            if ($request->hasFile('experiencecertificate1')) {
                //  Let's do everything here
                if ($request->file('experiencecertificate1')->isValid()) {
                    //
                    $validated = $request->validate([
                        
                        'experiencecertificate1' => 'mimes:jpeg,png,jpg,gif,svg|max:4000',
                    ]);

                    $backenddeveloper = new Backenddeveloper();

                    $lastbackenddeveloperID = $backenddeveloper->orderBy('id', 'DESC')->pluck('id')->first();
                    $newID = $lastbackenddeveloperID + 1;

                    $extension = $request->experiencecertificate1->extension();
                    $request->experiencecertificate1->storeAs('/public/backend/experiencecertificate1', $newID.".".$extension);
                    $url = Storage::url($newID.".".$extension);

                    // $Jobapplication->url=$url;
                    $Backenddeveloper->experiencecertificate1URL = $newID.".".$extension;
                    // $Jobapplication->url= '/public/upload/'
                    $imgsuccess =1;
                }
            }

            if($imgsuccess !=1){
                abort(500, 'Could not upload image :(');
            }

            if ($request->hasFile('experiencecertificate2')) {
                //  Let's do everything here
                if ($request->file('experiencecertificate2')->isValid()) {
                    //
                    $validated = $request->validate([
                        
                        'experiencecertificate2' => 'mimes:jpeg,png,jpg,gif,svg|max:4000',
                    ]);

                    $backenddeveloper = new Backenddeveloper();

                    $lastbackenddeveloperID = $backenddeveloper->orderBy('id', 'DESC')->pluck('id')->first();
                    $newID = $lastbackenddeveloperID + 1;

                    $extension = $request->experiencecertificate2->extension();
                    $request->experiencecertificate2->storeAs('/public/backend/experiencecertificate2', $newID.".".$extension);
                    $url = Storage::url($newID.".".$extension);

                    // $Jobapplication->url=$url;
                    $Backenddeveloper->experiencecertificate2URL = $newID.".".$extension;
                    // $Jobapplication->url= '/public/upload/'
                    $imgsuccess =1;
                }
            }

            if($imgsuccess !=1){
                abort(500, 'Could not upload image :(');
            }

            if ($request->hasFile('experiencecertificate3')) {
                //  Let's do everything here
                if ($request->file('experiencecertificate3')->isValid()) {
                    //
                    $validated = $request->validate([
                        
                        'experiencecertificate3' => 'mimes:jpeg,png,jpg,gif,svg|max:4000',
                    ]);

                    $backenddeveloper = new Backenddeveloper();

                    $lastbackenddeveloperID = $backenddeveloper->orderBy('id', 'DESC')->pluck('id')->first();
                    $newID = $lastbackenddeveloperID + 1;

                    $extension = $request->experiencecertificate3->extension();
                    $request->experiencecertificate3->storeAs('/public/backend/experiencecertificate3', $newID.".".$extension);
                    $url = Storage::url($newID.".".$extension);

                    // $Jobapplication->url=$url;
                    $Backenddeveloper->experiencecertificate3URL = $newID.".".$extension;
                    // $Jobapplication->url= '/public/upload/'
                    $imgsuccess =1;
                }
            }

            if($imgsuccess !=1){
                abort(500, 'Could not upload image :(');
            }

            if ($request->hasFile('experiencecertificate4')) {
                //  Let's do everything here
                if ($request->file('experiencecertificate4')->isValid()) {
                    //
                    $validated = $request->validate([
                        
                        'experiencecertificate4' => 'mimes:jpeg,png,jpg,gif,svg|max:4000',
                    ]);

                    $backenddeveloper = new Backenddeveloper();

                    $lastbackenddeveloperID = $backenddeveloper->orderBy('id', 'DESC')->pluck('id')->first();
                    $newID = $lastbackenddeveloperID + 1;

                    $extension = $request->experiencecertificate4->extension();
                    $request->experiencecertificate4->storeAs('/public/backend/experiencecertificate4', $newID.".".$extension);
                    $url = Storage::url($newID.".".$extension);

                    // $Jobapplication->url=$url;
                    $Backenddeveloper->experiencecertificate4URL = $newID.".".$extension;
                    // $Jobapplication->url= '/public/upload/'
                    $imgsuccess =1;
                }
            }

            if($imgsuccess !=1){
                abort(500, 'Could not upload image :(');
            }

            if ($request->hasFile('experiencecertificate5')) {
                //  Let's do everything here
                if ($request->file('experiencecertificate5')->isValid()) {
                    //
                    $validated = $request->validate([
                        
                        'experiencecertificate5' => 'mimes:jpeg,png,jpg,gif,svg|max:4000',
                    ]);

                    $backenddeveloper = new Backenddeveloper();

                    $lastbackenddeveloperID = $backenddeveloper->orderBy('id', 'DESC')->pluck('id')->first();
                    $newID = $lastbackenddeveloperID + 1;

                    $extension = $request->experiencecertificate5->extension();
                    $request->experiencecertificate5->storeAs('/public/backend/experiencecertificate5', $newID.".".$extension);
                    $url = Storage::url($newID.".".$extension);

                    // $Jobapplication->url=$url;
                    $Backenddeveloper->experiencecertificate5URL = $newID.".".$extension;
                    // $Jobapplication->url= '/public/upload/'
                    $imgsuccess =1;
                }
            }

            if($imgsuccess !=1){
                abort(500, 'Could not upload image :(');
            }

            //-->End of Experience Certificate Upload

            //-> Start of Project Portfolio Upload 
            if($request->jobapply != 11) {
                if ($request->hasFile('portfolio')) {
                    //  Let's do everything here
                    if ($request->file('portfolio')->isValid()) {
                        //
                        $validated = $request->validate([
                            
                            'portfolio' => 'mimes:jpeg,png,jpg,gif,svg,pdf|max:4000',
                        ]);

                        $backenddeveloper = new Backenddeveloper();

                        $lastbackenddeveloperID = $backenddeveloper->orderBy('id', 'DESC')->pluck('id')->first();
                        $newID = $lastbackenddeveloperID + 1;

                        $extension = $request->portfolio->extension();
                        $request->portfolio->storeAs('/public/backend/portfolio', $newID.".".$extension);
                        $url = Storage::url($newID.".".$extension);

                        // $Jobapplication->url=$url;
                        $Backenddeveloper->portfolioURL = $newID.".".$extension;
                        // $Jobapplication->url= '/public/upload/'
                        $imgsuccess =1;
                    }
                }

                if($imgsuccess !=1){
                    abort(500, 'Could not upload image :(');
                }

            }

            //--> End of Project Portfolio Upload

            

                // $fileName = time().'.'.$request->file->extension();  
                // $fileName = time();  
                // $request->signature->move(public_path('uploads'), $fileName);

                $Backenddeveloper->save();

                if($request->jobapply == 1)
                {
                     $Backenddeveloper->registrationNo = 'PM-REG-';
                }
                
                if($request->jobapply == 2)
                {
                     $Backenddeveloper->registrationNo = 'SMC-REG-';
                }

                
                if($request->jobapply == 3)
                {
                     $Backenddeveloper->registrationNo = 'DA-REG-';
                }

                
                if($request->jobapply == 4)
                {
                     $Backenddeveloper->registrationNo = 'SA-REG-';
                }

                
                if($request->jobapply == 5)
                {
                     $Backenddeveloper->registrationNo = 'NSA-REG-';
                }

                
                if($request->jobapply == 6)
                {
                     $Backenddeveloper->registrationNo = 'UX-REG-';
                }

                
                if($request->jobapply == 7)
                {
                     $Backenddeveloper->registrationNo = 'SDF-REG-';
                }

                
                if($request->jobapply == 8)
                {
                     $Backenddeveloper->registrationNo = 'SDB-REG-';
                }

                
                if($request->jobapply == 9)
                {
                     $Backenddeveloper->registrationNo = 'MM-REG-';
                }

                
                if($request->jobapply == 10)
                {
                     $Backenddeveloper->registrationNo = 'MT-REG-';
                }

                
                if($request->jobapply == 11)
                {
                     $Backenddeveloper->registrationNo = 'CO-REG-';
                }
               
                $Backenddeveloper->registrationNo .= $Backenddeveloper->id;
                


                $Backenddeveloper->save();


            //     $client = new Client();
            //     $response=$client->request ('POST','https://sms.mizoram.gov.in/api', [
            //       'form_params' => [
            //           'api_key' => 'b53366c91585c976e6173e69f6916b2d',
            //           'number' => $request->phone,
            //           'message' => 'MSeGS: Your Application Form submitted successfull. Please save your Registration No : '.$Backenddeveloper->registrationNo,
            //           // 'number' => $phone_number,
            //           // 'message' => $mOTP . ' is your OTP from eTender, MSeGS',
            //       ]
            //   ]);

                $applicant = $Backenddeveloper;


                return view('jobapplication.success',compact('applicant'));
                // return redirect('/success',compact('applicant'));



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Backenddeveloper  $backenddeveloper
     * @return \Illuminate\Http\Response
     */
    public function show(Backenddeveloper $backenddeveloper)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Backenddeveloper  $backenddeveloper
     * @return \Illuminate\Http\Response
     */
    public function edit(Backenddeveloper $backenddeveloper)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Backenddeveloper  $backenddeveloper
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Backenddeveloper $backenddeveloper)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Backenddeveloper  $backenddeveloper
     * @return \Illuminate\Http\Response
     */

    public function deletePage($id)
    {   
        $applicant = Backenddeveloper::find($id);
        return view('jobapplication.deletePage',compact('id','applicant'));
    }
    public function destroy(Request $request)
    {
        // dd($id,"For Security purpose, please counsult the Developer to delete the applicant");
        if($request->code == '007')   {
        $applicant = Backenddeveloper::find($request->id);

            // $deletejob = Backenddeveloper::find($id)->replicate()->setTable('deletejobs')->save();  deletejob hi boolean a nih vangin a hnuai a mi ang khuan kan ti ang
            $deletejob = Backenddeveloper::find($request->id)->replicate()->setTable('deletejobs');
            $deletejob->previous_id = $request->id;
            $deletejob->save();
            // $deletejob->previous_id = $applicant->id;
            // $deletejob->save();



            $applicant->delete(); //delete the applicant

            return redirect()->route('applicantList')->with('message', 'Applicant Deleted Successfully');
        }

            // $Backenddeveloper = Backenddeveloper::orderBy('id')->paginate();
            // $total = DB::table('backenddevelopers')->count();
            // $pending = DB::table('backenddevelopers')->where('status','0')->count();
            // $accept = DB::table('backenddevelopers')->where('status','1')->count();
            // $reject = DB::table('backenddevelopers')->where('status','2')->count();
            return redirect()->route('applicantList')->with('message', 'Wrong Admin Code');
           
        // $accept = DB::table('accept')->where('status','1')->where('division_id','2')->count();
        
              
            // return redirect()->route('admin.clients');
      
    }
    public function msegs($id)
    {
        $applicant = Backenddeveloper::find($id);

    
        $applicant->delete(); //delete the applicant
        return redirect()->route('applicantList');
    }
}
