<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Backenddeveloper extends Model
{
    protected $fillable = [

        'name', 'passportURL','email','gender', 'phone','whatsapp','religion','dob','fathersname','caste',
        'state','district','caddress',
        
        'subject1','university1','year1','marksheet1URL','passingcertificate1URL',
        'subject2','university2','year2','marksheet2URL','passingcertificate2URL',
        'subject3','university3','year3','marksheet3URL','passingcertificate3URL',
        'subject4','university4','year4','marksheet4URL','passingcertificate4URL',

        'coursename1','period1','institute1',
        'coursename2','period2','institute2',
        'coursename3','period3','institute3',

        'postname1','organizationname1','duration1','experiencecertificate1URL',
        'postname2','organizationname2','duration2','experiencecertificate2URL',
        'postname3','organizationname3','duration3','experiencecertificate3URL',
        'postname4','organizationname4','duration4','experiencecertificate4URL',
        'postname5','organizationname5','duration5','experiencecertificate5URL',

        'technicalknowledges',
        'portfolioURL',
        'jobapplys',
        'registrationNo',
        'status',


    ];
}
